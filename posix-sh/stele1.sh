#!/bin/sh

set -e

cmd="$(basename "$0")"

for dep in uuidgen exiftool # jq
do
	if ! hash "$dep" >/dev/null 2>/dev/null
	then
		printf '%s is required, but can not be found' "$dep" >&2
		exit 1
	fi
done

die(){
	printf 'ABORTING: ' >&2
	printf "$@" >&2
	printf '\n' >&2
	exit 1
}

short_help(){
	printf "usage: %s create PROJECT_PATH [NAME [DESCRIPTION]]\n" "$cmd"
	printf "usage: %s exists PROJECT_PATH\n" "$cmd"
	printf "usage: %s add-photo    PROJECT_PATH IMAGE_PATH\n" "$cmd"
	printf "usage: %s add-climb    PROJECT_PATH NAME\n" "$cmd"
	printf "usage: %s add-area     PROJECT_PATH NAME\n" "$cmd"
	printf "usage: %s add-parking  PROJECT_PATH NAME\n" "$cmd"
	printf "usage: %s add-approach PROJECT_PATH NAME\n" "$cmd"
}

project_exists(){
	if [ ! -d "$1" ]; then
		return 1
	elif [ ! -f "${1}/stele1.txt" ]; then
		return 1
	elif [ ! -f "${1}/project.json" ]; then
		return 1
	else
		return 0
	fi
}

cmd_create(){

	if [ ! -d "$(dirname "$1")" ]
	then
		die 'parent directory missing %s\n' "$(dirname "$1")"
	fi

	if [ -d "$1" ]
	then
		die 'directory already exists %s\n' "$1"
	fi

	mkdir "${1}"
	mkdir "${1}/climbs"
	mkdir "${1}/photos"
	mkdir "${1}/areas"
	mkdir "${1}/parkings"
	mkdir "${1}/approaches"
	mkdir "${1}/extensions"

	echo 'stele1' > "${1}/stele1.txt"

	cat > "${1}/project.json" <<-__EOF__
		{
		    "authors": [],
		    "description": "${3:-undescribed}",
		    "name": "${2:-unnamed}",
		    "uuid": "$(uuidgen)"
		}
	__EOF__
}

cmd_exists(){
	project_exists "$1"
}

cmd_add_photo(){

	if [ $# -ne 2 ]; then
		die 'two arguments required to add photo, got %s' $#
	fi

	photo_uuid="$(uuidgen)"
	extn="$(exiftool -p '$fileTypeExtension' "$2")"

	if ! project_exists "$1"; then
		die 'no project found at %s' "$1"
	fi

	target_photo="${1%/}/photos/${photo_uuid}_image.${extn}" 
	mkdir -p "${1%/}/photos"
	cp "$2" "$target_photo"
	cat > "${1%/}/photos/${photo_uuid}.json" <<-__END__
		{
		    "uuid": "${photo_uuid}",
		    "fields": {
		        "original_filename": "$(basename "$2")",
		        "added_iso_timestamp": "$(date --iso-8601=seconds)"
		    }
		}
	__END__
	echo "$photo_uuid"

}

cmd_add_climb(){
	if [ $# -ne 2 ]; then
		die 'two arguments required to add climb, got %s' $#
	fi

	climb_uuid="$(uuidgen)"

	if ! project_exists "$1"; then
		die 'no project found at %s' "$1"
	fi

	cat > "${1}/climbs/${climb_uuid}.json" <<-__EOF__
		{
		    "name": "${2:-unnamed}",
		    "uuid": "${climb_uuid}",
		    "fields": {
		        "added_iso_timestamp": "$(date --iso-8601=seconds)"
		    }
		}
	__EOF__
	echo "$climb_uuid"
}

cmd_add_area(){
	area_uuid="$(uuidgen)"
	cat > "${1}/areas/${area_uuid}.json" <<-__EOF__
		{
		    "name": "${2:-unnamed}",
		    "uuid": "${area_uuid}"
		}
	__EOF__
	echo "$area_uuid"
}

cmd_add_parking(){
	parking_uuid="$(uuidgen)"
	cat > "${1}/parkings/${parking_uuid}.json" <<-__EOF__
		{
		    "name": "${2:-unnamed}",
		    "uuid": "${parking_uuid}"
		}
	__EOF__
	echo "$parking_uuid"
}

cmd_add_approach(){
	approach_uuid="$(uuidgen)"
	cat > "${1}/approaches/${approach_uuid}.json" <<-__EOF__
		{
		    "name": "${2:-unnamed}",
		    "uuid": "${approach_uuid}"
		}
	__EOF__
	echo "$approach_uuid"
}

if [ $# -eq 0 ]
then
	short_help
	exit
fi

case $1 in
	help|h|--help|-h) short_help; exit 0 ;;
	create)           shift; cmd_create "$@" ;;
	exists)           shift; cmd_exists "$@" ;;
	add-photo)        shift; cmd_add_photo "$@" ;;
	add-climb)        shift; cmd_add_climb "$@" ;;
	add-area)         shift; cmd_add_area "$@" ;;
	add-parking)      shift; cmd_add_parking "$@" ;;
	add-approach)     shift; cmd_add_approach "$@" ;;
	*)
		short_help >&2
		die 'unknown subcommand "%s"' "$1"
esac
