#!/usr/bin/env python3

import setuptools

setuptools.setup(
    name="stele1-filesystem",
    version="1.0.0",
    author="Daniel M",
    author_email="dan.mntg@gmail.com",
    description="store and manage stele1 data on the filesystem",
    long_description="store and manage stele1 data on the filesystem", # TODO: add more
    long_description_content_type="text/plain",
    url="https://gitlab.com/stele-climbing/stele1",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Development Status :: 5 - Production/Stable",
    ],
    python_requires='>=3.6',
)
