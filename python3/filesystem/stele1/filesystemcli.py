import os
import os.path
import sys
import uuid
import stele1.filesystem

import stele1.climb as climb
import stele1.approach as approach
import stele1.photo as photo
import stele1.parking as parking
import stele1.area as area
import stele1.metadata as metadata

def list_climbs(args):
    print('listing climbs')
    for id in project.climb_uuids():
        print(id.to_data())
def add_climb(args):
    e = stele1.climb.Climb()
    id = stele1.climb.Uuid.from_data(str(uuid.uuid4()))
    e.setUuid(id)
    project.set_climb(d)
    print(id.to_data())
def get_climb_by_uuid(args):
    print('getting climb')
def set_climb(args):
    print('setting climb')
def remove_climb_by_uuid(args):
    print('removing climb')

def list_approaches(args):
    print('listing approaches')
    for id in project.approach_uuids():
        print(id.to_data())
def add_approach(args):
    e = stele1.approach.Approach()
    id = stele1.approach.Uuid.from_data(str(uuid.uuid4()))
    e.setUuid(id)
    project.set_approach(d)
    print(id.to_data())
def get_approach_by_uuid(args):
    print('getting approach')
def set_approach(args):
    print('setting approach')
def remove_approach_by_uuid(args):
    print('removing approach')

def list_photos(args):
    print('listing photos')
    for id in project.photo_uuids():
        print(id.to_data())
def add_photo(args):
    e = stele1.photo.Photo()
    id = stele1.photo.Uuid.from_data(str(uuid.uuid4()))
    e.setUuid(id)
    project.set_photo(d)
    print(id.to_data())
def get_photo_by_uuid(args):
    print('getting photo')
def set_photo(args):
    print('setting photo')
def remove_photo_by_uuid(args):
    print('removing photo')

def list_parkings(args):
    print('listing parkings')
    for id in project.parking_uuids():
        print(id.to_data())
def add_parking(args):
    e = stele1.parking.Parking()
    id = stele1.parking.Uuid.from_data(str(uuid.uuid4()))
    e.setUuid(id)
    project.set_parking(d)
    print(id.to_data())
def get_parking_by_uuid(args):
    print('getting parking')
def set_parking(args):
    print('setting parking')
def remove_parking_by_uuid(args):
    print('removing parking')

def list_areas(args):
    print('listing areas')
    for id in project.area_uuids():
        print(id.to_data())
def add_area(args):
    e = stele1.area.Area()
    id = stele1.area.Uuid.from_data(str(uuid.uuid4()))
    e.setUuid(id)
    project.set_area(d)
    print(id.to_data())
def get_area_by_uuid(args):
    print('getting area')
def set_area(args):
    print('setting area')
def remove_area_by_uuid(args):
    print('removing area')

def main():
    subcmd = sys.argv[1]
    subcmd_args = sys.argv[2:]
    proj = stele1.filesystem.Project(os.environ['STELE1_PROJECT']) if 'STELE1_PROJECT' in os.environ else None
    print(proj)
    if subcmd == 'help':
        print(f'usage: {os.path.basename(sys.argv[0])} SUBCOMMAND ...')
        print('subcommands:')
        print('    create, exists, get-metadata, set-metadata')
        print('    list-climbs add-climb ' +
            'get-climb set-climb remove-climb')
        print('    list-approaches add-approach ' +
            'get-approach set-approach remove-approach')
        print('    list-photos add-photo ' +
            'get-photo set-photo remove-photo')
        print('    list-parkings add-parking ' +
            'get-parking set-parking remove-parking')
        print('    list-areas add-area ' +
            'get-area set-area remove-area')
        print('environment vars:')
        print('    STELE1_PROJECT')
    elif subcmd == 'exists':
        print('TODO: exists')
    elif subcmd == 'create':
        print('TODO: create')
    elif subcmd == 'set-metadata':
        print('TODO: set metadata')
    elif subcmd == 'list-climbs':
        list_climbs(subcmd_args)
    elif subcmd == 'add-climb':
        add_climb(subcmd_args)
    elif subcmd == 'get-climb':
        get_climb(subcmd_args)
    elif subcmd == 'set-climb':
        set_climb(subcmd_args)
    elif subcmd == 'remove-climb':
        remove_climb(subcmd_args)
    elif subcmd == 'list-approaches':
        list_approaches(subcmd_args)
    elif subcmd == 'add-approach':
        add_approach(subcmd_args)
    elif subcmd == 'get-approach':
        get_approach(subcmd_args)
    elif subcmd == 'set-approach':
        set_approach(subcmd_args)
    elif subcmd == 'remove-approach':
        remove_approach(subcmd_args)
    elif subcmd == 'list-photos':
        list_photos(subcmd_args)
    elif subcmd == 'add-photo':
        add_photo(subcmd_args)
    elif subcmd == 'get-photo':
        get_photo(subcmd_args)
    elif subcmd == 'set-photo':
        set_photo(subcmd_args)
    elif subcmd == 'remove-photo':
        remove_photo(subcmd_args)
    elif subcmd == 'list-parkings':
        list_parkings(subcmd_args)
    elif subcmd == 'add-parking':
        add_parking(subcmd_args)
    elif subcmd == 'get-parking':
        get_parking(subcmd_args)
    elif subcmd == 'set-parking':
        set_parking(subcmd_args)
    elif subcmd == 'remove-parking':
        remove_parking(subcmd_args)
    elif subcmd == 'list-areas':
        list_areas(subcmd_args)
    elif subcmd == 'add-area':
        add_area(subcmd_args)
    elif subcmd == 'get-area':
        get_area(subcmd_args)
    elif subcmd == 'set-area':
        set_area(subcmd_args)
    elif subcmd == 'remove-area':
        remove_area(subcmd_args)
    else:
        raise Exception('unknown subcommand ' + subcmd)
