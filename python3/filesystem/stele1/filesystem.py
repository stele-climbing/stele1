import shutil
import os
import os.path
import json

import stele1.climb as climb
import stele1.approach as approach
import stele1.photo as photo
import stele1.parking as parking
import stele1.area as area
import stele1.metadata as metadata

class Project:

    def __init__(self, path):
        self._path_original = path;
        self._path = os.path.abspath(path);
    def _subpath(self, *segments):
        return os.path.join(self._path, *segments)
    def _ensure_dir(self, path):
        if not os.path.isdir(path):
            os.mkdir(path)
        return None
    def _ensure_parent_dir(self, path):
        return self._ensure_dir(os.path.dirname(path))

    def exists(self):
        isfile = os.path.isfile
        metafile = self._subpath('project.json')
        versionfile = self._subpath('stele1.txt')
        return isfile(metafile) and isfile(versionfile)

    def create(self):
        parent = os.path.dirname(self._path)
        if os.path.isdir(self._path):
            raise Exception(f'directory exists {self._path}')
        if not os.path.isdir(parent):
            raise Exception(f'parent directory missing {parent} (for {self._path})')
        os.mkdir(self._path)
        os.mkdir(self._subpath('climbs'))
        os.mkdir(self._subpath('approaches'))
        os.mkdir(self._subpath('photos'))
        os.mkdir(self._subpath('parkings'))
        os.mkdir(self._subpath('areas'))
        with open(os.path.join(self._path, 'stele1.txt'), 'w') as f:
            f.write('stele1' + os.linesep)
        self.set_metadata(metadata.Metadata())

    def get_metadata(self):
        metapath = self._subpath('project.json')
        if os.path.isfile(metapath):
            with open(metapath, 'r') as f:
                try:
                    return metadata.Metadata.from_data(json.load(f))
                except json.decoder.JSONDecodeError as e:
                    raise Exception(f'bad json at {pat}: {e.args[0]}')
        else:
            return None
    def set_metadata(self, metadata):
        metapath = self._subpath('project.json')
        if os.path.isdir(os.path.dirname(metapath)):
            with open(metapath, 'w') as f:
                json.dump(metadata.to_data(), f, indent=4, ensure_ascii=False)
                f.write(os.linesep)
        else:
            return None

    def climb_uuids(self):
        dir = self._subpath('climbs')
        if os.path.isdir(dir):
            for f in os.listdir(dir):
                if f.endswith('.json'):
                    yield climb.Uuid.from_data(f.replace('.json', ''))
    def get_climb_by_uuid(self, uuid):
        pat = self._subpath('climbs', uuid.to_data() + '.json')
        if os.path.isfile(pat):
            with open(pat, 'r') as f:
                try:
                    e = climb.Climb.from_data(json.load(f)) 
                    if uuid.to_data() != e.get_uuid().to_data():
                        raise Exception(f'path {pat} does not match the uuid {e.get_uuid().to_data()}')
                    return e
                except json.decoder.JSONDecodeError as e:
                    raise Exception(f'bad json at {pat}: {e.args[0]}')
    def set_climb(self, climb):
        uuid = climb.get_uuid()
        if not uuid:
            raise Exception('can not use climb without a uuid')
        pat = self._subpath('climbs', climb.get_uuid().to_data() + '.json')
        self._ensure_parent_dir(pat)
        with open(pat, 'w') as f:
            json.dump(climb.to_data(), f, indent=4, ensure_ascii=False)
            f.write(os.linesep)
    def remove_climb_by_uuid(self, uuid):
        pat = self._subpath('climbs', uuid.to_data() + '.json')
        if os.path.isfile(pat):
            os.unlink(pat)

    def approach_uuids(self):
        dir = self._subpath('approaches')
        if os.path.isdir(dir):
            for f in os.listdir(dir):
                if f.endswith('.json'):
                    yield approach.Uuid.from_data(f.replace('.json', ''))
    def get_approach_by_uuid(self, uuid):
        pat = self._subpath('approaches', uuid.to_data() + '.json')
        if os.path.isfile(pat):
            with open(pat, 'r') as f:
                try:
                    e = approach.Approach.from_data(json.load(f)) 
                    if uuid.to_data() != e.get_uuid().to_data():
                        raise Exception(f'path {pat} does not match the uuid {e.get_uuid().to_data()}')
                    return e
                except json.decoder.JSONDecodeError as e:
                    raise Exception(f'bad json at {pat}: {e.args[0]}')
    def set_approach(self, approach):
        uuid = approach.get_uuid()
        if not uuid:
            raise Exception('can not use approach without a uuid')
        pat = self._subpath('approaches', approach.get_uuid().to_data() + '.json')
        self._ensure_parent_dir(pat)
        with open(pat, 'w') as f:
            json.dump(approach.to_data(), f, indent=4, ensure_ascii=False)
            f.write(os.linesep)
    def remove_approach_by_uuid(self, uuid):
        pat = self._subpath('approaches', uuid.to_data() + '.json')
        if os.path.isfile(pat):
            os.unlink(pat)

    def photo_uuids(self):
        dir = self._subpath('photos')
        if os.path.isdir(dir):
            for f in os.listdir(dir):
                if f.endswith('.json'):
                    yield photo.Uuid.from_data(f.replace('.json', ''))
    def get_photo_by_uuid(self, uuid):
        pat = self._subpath('photos', uuid.to_data() + '.json')
        if os.path.isfile(pat):
            with open(pat, 'r') as f:
                try:
                    e = photo.Photo.from_data(json.load(f)) 
                    if uuid.to_data() != e.get_uuid().to_data():
                        raise Exception(f'path {pat} does not match the uuid {e.get_uuid().to_data()}')
                    return e
                except json.decoder.JSONDecodeError as e:
                    raise Exception(f'bad json at {pat}: {e.args[0]}')
    def set_photo(self, photo):
        uuid = photo.get_uuid()
        if not uuid:
            raise Exception('can not use photo without a uuid')
        pat = self._subpath('photos', photo.get_uuid().to_data() + '.json')
        self._ensure_parent_dir(pat)
        with open(pat, 'w') as f:
            json.dump(photo.to_data(), f, indent=4, ensure_ascii=False)
            f.write(os.linesep)
    def remove_photo_by_uuid(self, uuid):
        pat = self._subpath('photos', uuid.to_data() + '.json')
        imgpat = self._get_photo_image_path(uuid)
        if os.path.isfile(pat):
            os.unlink(pat)
        if os.path.isfile(imgpat):
            os.unlink(imgpat)
    def _get_photo_image_path(self, uuid):
        dir = self._subpath('photos')
        prefix = uuid.to_data() + '_image'
        if os.path.isdir(dir):
            for f in os.listdir(dir):
                if f.startswith(prefix):
                    return os.path.join(dir, f)
    def set_photo_image_by_uuid(self, uuid, path):
        if not os.path.isfile(path):
            raise Exception('no file at path ' + path)
        dir = self._subpath('photos')
        existing = self._get_photo_image_path(uuid)
        ext = os.path.basename(path).split('.')[-1]
        suffix = '.' + ext if ext else ''
        if existing:
            # TODO: warn the user because this is a terrible idea
            os.unlink(existing)
        shutil.copyfile(path, os.path.join(dir, f'{uuid.to_data()}_image{suffix}'))
    def get_photo_image_by_uuid(self, uuid):
        return self._get_photo_image_path(uuid)

    def parking_uuids(self):
        dir = self._subpath('parkings')
        if os.path.isdir(dir):
            for f in os.listdir(dir):
                if f.endswith('.json'):
                    yield parking.Uuid.from_data(f.replace('.json', ''))
    def get_parking_by_uuid(self, uuid):
        pat = self._subpath('parkings', uuid.to_data() + '.json')
        if os.path.isfile(pat):
            with open(pat, 'r') as f:
                try:
                    e = parking.Parking.from_data(json.load(f)) 
                    if uuid.to_data() != e.get_uuid().to_data():
                        raise Exception(f'path {pat} does not match the uuid {e.get_uuid().to_data()}')
                    return e
                except json.decoder.JSONDecodeError as e:
                    raise Exception(f'bad json at {pat}: {e.args[0]}')
    def set_parking(self, parking):
        uuid = parking.get_uuid()
        if not uuid:
            raise Exception('can not use parking without a uuid')
        pat = self._subpath('parkings', parking.get_uuid().to_data() + '.json')
        self._ensure_parent_dir(pat)
        with open(pat, 'w') as f:
            json.dump(parking.to_data(), f, indent=4, ensure_ascii=False)
            f.write(os.linesep)
    def remove_parking_by_uuid(self, uuid):
        pat = self._subpath('parkings', uuid.to_data() + '.json')
        if os.path.isfile(pat):
            os.unlink(pat)

    def area_uuids(self):
        dir = self._subpath('areas')
        if os.path.isdir(dir):
            for f in os.listdir(dir):
                if f.endswith('.json'):
                    yield area.Uuid.from_data(f.replace('.json', ''))
    def get_area_by_uuid(self, uuid):
        pat = self._subpath('areas', uuid.to_data() + '.json')
        if os.path.isfile(pat):
            with open(pat, 'r') as f:
                try:
                    e = area.Area.from_data(json.load(f)) 
                    if uuid.to_data() != e.get_uuid().to_data():
                        raise Exception(f'path {pat} does not match the uuid {e.get_uuid().to_data()}')
                    return e
                except json.decoder.JSONDecodeError as e:
                    raise Exception(f'bad json at {pat}: {e.args[0]}')
    def set_area(self, area):
        uuid = area.get_uuid()
        if not uuid:
            raise Exception('can not use area without a uuid')
        pat = self._subpath('areas', area.get_uuid().to_data() + '.json')
        self._ensure_parent_dir(pat)
        with open(pat, 'w') as f:
            json.dump(area.to_data(), f, indent=4, ensure_ascii=False)
            f.write(os.linesep)
    def remove_area_by_uuid(self, uuid):
        pat = self._subpath('areas', uuid.to_data() + '.json')
        if os.path.isfile(pat):
            os.unlink(pat)

if __name__ == '__main__':
    import stele1.filesystemcli
    stele1.filesystemcli.main()
