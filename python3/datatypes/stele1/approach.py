class Name:
    def __init__(self, n):
        if not isinstance(n, str):
            raise TypeError("approach name is not a string")
        if n == "":
            raise ValueError("approach name is an empty string")
        self._name = n
    @staticmethod
    def from_data(j):
        return Name(j)
    def to_data(self):
        return self._name

class Description:
    def __init__(self, description):
        if not isinstance(description, str):
            raise TypeError("approach description is not a string")
        if description == "":
            raise ValueError("approach description is an empty string")
        self._description = description
    @staticmethod
    def from_data(j):
        return Description(j)
    def to_data(self):
        return self._description

class Path:
    def __init__(self, points):
        if not isinstance(points, list):
            raise TypeError("approach path must be a list")
        if len(points) < 2:
            raise TypeError("approach path must have a least two points")
        good = []
        for longitude, latitude in points:
            if not isinstance(longitude, (int, float)):
                raise TypeError("approach path points' longitude must be a number")
            if not isinstance(latitude, (int, float)):
                raise TypeError("approach path points' latitude must be a number")
            if longitude < -180 or 180 < longitude:
                raise ValueError("approach path points' longitude must be between -180 and 180")
            if latitude < -90 or 90 < latitude:
                raise ValueError("approach location's latitude must be between -90 and 90")
            good.append((longitude, latitude))
        self._points = good
    @staticmethod
    def from_data(j):
        return Path([(pt['longitude'], pt['latitude']) for pt in j])
    def to_data(self):
        return [{'longitude': lng, 'latitude': lat} for (lng, lat) in  self._points]

class Uuid:
    def __init__(self, a):
        import uuid
        if isinstance(a, str):
            self._uuid = uuid.UUID(a)
        elif isinstance(a, uuid.UUID):
            self._uuid = a
        else:
            raise TypeError("approach uuid must be a string or UUID")
    @staticmethod
    def from_data(a):
        import re
        rex = re.compile('[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}')
        if not isinstance(a, str):
            raise TypeError("approach uuid must be a string")
        if not rex.match(a):
            raise ValueError("approach uuid string must look like a uuid: " +
                             "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx")
        return Uuid(a)
    def to_data(self):
        return str(self._uuid)

class Approach:

    def __init__(self):
        self._description = None
        self._name = None
        self._path = None
        self._uuid = None

    @staticmethod
    def from_data(d):
        a = Approach()
        for k, v in d.items():
            if k == 'description':
                a._description = Description.from_data(v)
            elif k == 'name':
                a._name = Name.from_data(v)
            elif k == 'path':
                a._path = Path.from_data(v)
            elif k == 'uuid':
                a._uuid = Uuid.from_data(v)
            else:
                raise ValueError('unexpected key ' + k)
        return a

    def to_data(self):
        d = {}
        if self._description:
            d['description'] = self._description.to_data()
        if self._name:
            d['name'] = self._name.to_data()
        if self._path:
            d['path'] = self._path.to_data()
        if self._uuid:
            d['uuid'] = self._uuid.to_data()
        return d

    def get_description(self):
        return self._description
    def unset_description(self):
        self._description = None
        return self
    def set_description(self, v):
        if not isinstance(v, Description):
            raise TypeError('arguments to set_description must be an instance of Description')
        self._description = v
        return self

    def get_name(self):
        return self._name
    def unset_name(self):
        self._name = None
        return self
    def set_name(self, v):
        if not isinstance(v, Name):
            raise TypeError('arguments to set_name must be an instance of Name')
        self._name = v
        return self

    def get_path(self):
        return self._path
    def unset_path(self):
        self._path = None
        return self
    def set_path(self, v):
        if not isinstance(v, Path):
            raise TypeError('arguments to set_path must be an instance of Path')
        self._path = v
        return self

    def get_uuid(self):
        return self._uuid
    def unset_uuid(self):
        self._uuid = None
        return self
    def set_uuid(self, v):
        if not isinstance(v, Uuid):
            raise TypeError('arguments to set_uuid must be an instance of Uuid')
        self._uuid = v
        return self

