class Name:
    def __init__(self, n):
        if not isinstance(n, str):
            raise TypeError("parking name is not a string")
        if n == "":
            raise ValueError("parking name is an empty string")
        self._name = n
    @staticmethod
    def from_data(j):
        return Name(j)
    def to_data(self):
        return self._name

class Description:
    def __init__(self, description):
        if not isinstance(description, str):
            raise TypeError("parking description is not a string")
        if description == "":
            raise ValueError("parking description is an empty string")
        self._description = description
    @staticmethod
    def from_data(j):
        return Description(j)
    def to_data(self):
        return self._description

class Capacity:
    def __init__(self, n):
        if not isinstance(n, int):
            raise TypeError("parking capacity must be an integer")
        if n <= 0:
            raise ValueError("parking capacity must be greater than 0")
        self._capacity = n
    @staticmethod
    def from_data(j):
        return Capacity(j)
    def to_data(self):
        return self._capacity

class Location:
    def __init__(self, longitude=None, latitude=None):
        if not isinstance(longitude, (int, float)):
            raise TypeError("parking location's longitude must be a number")
        if not isinstance(latitude, (int, float)):
            raise TypeError("parking location's latitude must be a number")
        if longitude < -180 or 180 < longitude:
            raise ValueError("parking location's longitude must be between -180 and 180")
        if latitude < -90 or 90 < latitude:
            raise ValueError("parking location's latitude must be between -90 and 90")
        self._longitude = longitude
        self._latitude = latitude
    @staticmethod
    def from_data(j):
        kwargs = {}
        if 'longitude' in j:
            kwargs['longitude'] = j['longitude']
        if 'latitude' in j:
            kwargs['latitude'] = j['latitude']
        return Location(**kwargs)
    def to_data(self):
        return {
            'longitude': self._longitude,
            'latitude': self._latitude,
        }

class Uuid:
    def __init__(self, a):
        import uuid
        if isinstance(a, str):
            self._uuid = uuid.UUID(a)
        elif isinstance(a, uuid.UUID):
            self._uuid = a
        else:
            raise TypeError("parking uuid must be a string or UUID")
    @staticmethod
    def from_data(a):
        import re
        rex = re.compile('[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}')
        if not isinstance(a, str):
            raise TypeError("parking uuid must be a string")
        if not rex.match(a):
            raise ValueError("parking uuid string must look like a uuid: " +
                             "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx")
        return Uuid(a)
    def to_data(self):
        return str(self._uuid)

class Parking:

    def __init__(self):
        self._capacity = None
        self._description = None
        self._location = None
        self._name = None
        self._uuid = None

    @staticmethod
    def from_data(d):
        a = Parking()
        for k, v in d.items():
            if k == 'capacity':
                a._capacity = Capacity.from_data(v)
            elif k == 'description':
                a._description = Description.from_data(v)
            elif k == 'location':
                a._location = Location.from_data(v)
            elif k == 'name':
                a._name = Name.from_data(v)
            elif k == 'uuid':
                a._uuid = Uuid.from_data(v)
            else:
                raise ValueError('unexpected key ' + k)
        return a

    def to_data(self):
        d = {}
        if self._capacity:
            d['capacity'] = self._capacity.to_data()
        if self._description:
            d['description'] = self._description.to_data()
        if self._location:
            d['location'] = self._location.to_data()
        if self._name:
            d['name'] = self._name.to_data()
        if self._uuid:
            d['uuid'] = self._uuid.to_data()
        return d

    def get_capacity(self):
        return self._capacity
    def unset_capacity(self):
        self._capacity = None
        return self
    def set_capacity(self, v):
        if not isinstance(v, Capacity):
            raise TypeError('arguments to set_capacity must be an instance of Capacity')
        self._capacity = v
        return self

    def get_description(self):
        return self._description
    def unset_description(self):
        self._description = None
        return self
    def set_description(self, v):
        if not isinstance(v, Description):
            raise TypeError('arguments to set_description must be an instance of Description')
        self._description = v
        return self

    def get_location(self):
        return self._location
    def unset_location(self):
        self._location = None
        return self
    def set_location(self, v):
        if not isinstance(v, Location):
            raise TypeError('arguments to set_location must be an instance of Location')
        self._location = v
        return self

    def get_name(self):
        return self._name
    def unset_name(self):
        self._name = None
        return self
    def set_name(self, v):
        if not isinstance(v, Name):
            raise TypeError('arguments to set_name must be an instance of Name')
        self._name = v
        return self

    def get_uuid(self):
        return self._uuid
    def unset_uuid(self):
        self._uuid = None
        return self
    def set_uuid(self, v):
        if not isinstance(v, Uuid):
            raise TypeError('arguments to set_uuid must be an instance of Uuid')
        self._uuid = v
        return self

