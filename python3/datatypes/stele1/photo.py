class RealDatetimeoriginal:
    def __init__(self, dto):
        if not isinstance(dto, str):
            raise TypeError("photo real-date-time-original must be a string")
        if dto == "":
            raise TypeError("photo real-date-time-original must be a non-empty string")
        self._dto = dto
    @staticmethod
    def from_data(j):
        return RealDatetimeoriginal(j)
    def to_data(self):
        return self._dto

class RealLocation:
    def __init__(self, longitude=None, latitude=None, radius=None):
        if not isinstance(longitude, (int, float)):
            raise TypeError("photo real-location's longitude must be a number")
        if not isinstance(latitude, (int, float)):
            raise TypeError("photo real-location's latitude must be a number")
        if not isinstance(radius, (int, float)):
            raise TypeError("photo real-location's meterRadius must be a number")
        if longitude < -180 or 180 < longitude:
            raise ValueError("photo real-location's longitude must be between -180 and 180")
        if latitude < -90 or 90 < latitude:
            raise ValueError("photo real-location's latitude must be between -90 and 90")
        if radius < 0:
            raise ValueError("photo real-location's meterRadius must not be negative")
        self._longitude = longitude
        self._latitude = latitude
        self._radius = radius
    @staticmethod
    def from_data(j):
        lng = j['longitude']
        lat = j['latitude']
        rad = j['meterRadius']
        return RealLocation(longitude=lng, latitude=lat, radius=rad)
    def to_data(self):
        return {
            'longitude': self._longitude,
            'latitude': self._latitude,
            'meterRadius': self._radius,
        }

class RealOrientation:
    def __init__(self, orientation):
        if not isinstance(orientation, int):
            raise TypeError("photo real-orientation must be an integer")
        if orientation not in {1, 2, 3, 4, 5, 6, 7, 8}:
            raise ValueError("photo real-orientation must be an integer from 1 to 8")
        self._dto = orientation
    @staticmethod
    def from_data(j):
        return RealOrientation(j)
    def to_data(self):
        return self._dto

class Fields:
    def __init__(self, fields):
        if not isinstance(fields, dict):
            raise TypeError("photo fields must be a dict")
        self._fields = {}
        for f, v in fields.items():
            if not isinstance(v, str):
                raise TypeError("photo field values must be a string")
            self._fields[f] = v
    @staticmethod
    def from_data(j):
        return Fields(j)
    def to_data(self):
        return self._fields.copy()

class ClimbLayer:
    def __init__(self, climb_uuid, path):
        import re
        rex = re.compile('[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}')
        if not isinstance(climb_uuid, str):
            raise TypeError('climb-layer.climb-uuid must be a string')
        if not rex.match(climb_uuid):
            raise ValueError("climb-layer.climb-uuid string must look like a uuid: " +
                             "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx")
        if not isinstance(path, list):
            raise TypeError('climb-layer path must be a list')
        if len(path) < 2:
            raise ValueError('climb-layer.path must have at least two points')
        self._climb_uuid = climb_uuid
        self._path = []
        for pt in path:
            if 'leftOffset' not in pt:
                raise ValueError('each point in climb-layer.path must have a leftOffset')
            if 'topOffset' not in pt:
                raise ValueError('each point in climb-layer.path must have a topOffset')
            if len(pt.keys()) != 2:
                raise ValueError('each point in climb-layer.path must have only a leftOffset and topOffset')
            self._path.append((pt['leftOffset'], pt['topOffset']))
    @staticmethod
    def from_data(d):
        if not isinstance(d, dict):
            raise TypeError("data for climb-layer must be a dict")
        if 'climbUuid' not in d:
            raise ValueError('climb-layer must have a climbUuid field')
        if 'path' not in d:
            raise ValueError('climb-layer must have a path field')
        if len(d.keys()) != 2:
            raise ValueError('climb-layer must have only a climbUuid and path')
        return ClimbLayer(d['climbUuid'], d['path'])
    def to_data(self):
        return {
            'climbUuid': self._climb_uuid,
            'path': [{'leftOffset': l, 'topOffset': t} for l, t in self._path]
        }
    def copy(self):
        return ClimbLayer.from_data(self.to_data())

class ClimbLayers:
    def __init__(self, layers):
        if not isinstance(layers, list):
            raise TypeError('climb-layers must be a list')
        self._layers = [l.copy() for l in layers]
    @staticmethod
    def from_data(d):
        if not isinstance(d, list):
            raise TypeError('climb-layers data must be a list')
        return ClimbLayers([ClimbLayer.from_data(l) for l in d])
    def to_data(self):
        return [l.to_data() for l in self._layers]

class AreaLayer:
    def __init__(self, area_uuid, polygon):
        import re
        rex = re.compile('[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}')
        if not isinstance(area_uuid, str):
            raise TypeError('area-layer.area-uuid must be a string')
        if not rex.match(area_uuid):
            raise ValueError("area-layer.area-uuid string must look like a uuid: " +
                             "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx")
        if not isinstance(polygon, list):
            raise TypeError('area-layer polygon must be a list')
        if len(polygon) < 4:
            raise ValueError('area-layer.polygon must have at least 4 points')
        self._area_uuid = area_uuid
        self._polygon = []
        for pt in polygon:
            if 'leftOffset' not in pt:
                raise ValueError('each point in area-layer.polygon must have a leftOffset')
            if 'topOffset' not in pt:
                raise ValueError('each point in area-layer.polygon must have a topOffset')
            if len(pt.keys()) != 2:
                raise ValueError('each point in area-layer.polygon must have only a leftOffset and topOffset')
            self._polygon.append((pt['leftOffset'], pt['topOffset']))
        x1, y1 = self._polygon[0]
        x2, y2 = self._polygon[-1]
        if x1 != x2 or y1 != y2:
            raise ValueError('the first and last point for an area-layer.polygon must be the same')
    @staticmethod
    def from_data(d):
        if not isinstance(d, dict):
            raise TypeError("data for area-layer must be a dict")
        if 'areaUuid' not in d:
            raise ValueError('area-layer must have a areaUuid field')
        if 'polygon' not in d:
            raise ValueError('area-layer must have a polygon field')
        if len(d.keys()) != 2:
            raise ValueError('area-layer must have only a areaUuid and polygon')
        return AreaLayer(d['areaUuid'], d['polygon'])
    def to_data(self):
        return {
            'areaUuid': self._area_uuid,
            'polygon': [{'leftOffset': l, 'topOffset': t} for l, t in self._polygon]
        }
    def copy(self):
        return AreaLayer.from_data(self.to_data())

class AreaLayers:
    def __init__(self, layers):
        if not isinstance(layers, list):
            raise TypeError('area-layers must be a list')
        self._layers = [l.copy() for l in layers]
    @staticmethod
    def from_data(d):
        if not isinstance(d, list):
            raise TypeError('area-layers data must be a list')
        return AreaLayers([AreaLayer.from_data(l) for l in d])
    def to_data(self):
        return [l.to_data() for l in self._layers]

class Uuid:
    def __init__(self, a):
        import uuid
        if isinstance(a, str):
            self._uuid = uuid.UUID(a)
        elif isinstance(a, uuid.UUID):
            self._uuid = a
        else:
            raise TypeError("photo uuid must be a string or UUID")
    @staticmethod
    def from_data(a):
        import re
        rex = re.compile('[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}')
        if not isinstance(a, str):
            raise TypeError("photo uuid must be a string")
        if not rex.match(a):
            raise ValueError("photo uuid string must look like a uuid: " +
                             "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx")
        return Uuid(a)
    def to_data(self):
        return str(self._uuid)

class Photo:

    def __init__(self):
        self._area_layers = None
        self._climb_layers = None
        self._fields = None
        self._real_datetimeoriginal = None
        self._real_location = None
        self._real_orientation = None
        self._uuid = None

    @staticmethod
    def from_data(d):
        a = Photo()
        for k, v in d.items():
            if k == 'areaLayers':
                a._area_layers = AreaLayers.from_data(v)
            elif k == 'climbLayers':
                a._climb_layers = ClimbLayers.from_data(v)
            elif k == 'fields':
                a._fields = Fields.from_data(v)
            elif k == 'realDatetimeoriginal':
                a._real_datetimeoriginal = RealDatetimeoriginal.from_data(v)
            elif k == 'realLocation':
                a._real_location = RealLocation.from_data(v)
            elif k == 'realOrientation':
                a._real_orientation = RealOrientation.from_data(v)
            elif k == 'uuid':
                a._uuid = Uuid.from_data(v)
            else:
                raise ValueError('unexpected key ' + k)
        return a

    def to_data(self):
        d = {}
        if self._area_layers:
            d['areaLayers'] = self._area_layers.to_data()
        if self._climb_layers:
            d['climbLayers'] = self._climb_layers.to_data()
        if self._fields:
            d['fields'] = self._fields.to_data()
        if self._real_datetimeoriginal:
            d['realDatetimeoriginal'] = self._real_datetimeoriginal.to_data()
        if self._real_location:
            d['realLocation'] = self._real_location.to_data()
        if self._real_orientation:
            d['realOrientation'] = self._real_orientation.to_data()
        if self._uuid:
            d['uuid'] = self._uuid.to_data()
        return d

    def get_area_layers(self):
        return self._area_layers
    def unset_area_layers(self):
        self._area_layers = None
        return self
    def set_area_layers(self, v):
        if not isinstance(v, AreaLayers):
            raise TypeError('arguments to set_area_layers must be an instance of AreaLayers')
        self._area_layers = v
        return self

    def get_climb_layers(self):
        return self._climb_layers
    def unset_climb_layers(self):
        self._climb_layers = None
        return self
    def set_climb_layers(self, v):
        if not isinstance(v, ClimbLayers):
            raise TypeError('arguments to set_climb_layers must be an instance of ClimbLayers')
        self._climb_layers = v
        return self

    def get_fields(self):
        return self._fields
    def unset_fields(self):
        self._fields = None
        return self
    def set_fields(self, v):
        if not isinstance(v, Fields):
            raise TypeError('arguments to set_fields must be an instance of Fields')
        self._fields = v
        return self

    def get_real_datetimeoriginal(self):
        return self._real_datetimeoriginal
    def unset_real_datetimeoriginal(self):
        self._real_datetimeoriginal = None
        return self
    def set_real_datetimeoriginal(self, v):
        if not isinstance(v, RealDatetimeoriginal):
            raise TypeError('arguments to set_real_datetimeoriginal must be an instance of RealDatetimeoriginal')
        self._real_datetimeoriginal = v
        return self

    def get_real_location(self):
        return self._real_location
    def unset_real_location(self):
        self._real_location = None
        return self
    def set_real_location(self, v):
        if not isinstance(v, RealLocation):
            raise TypeError('arguments to set_real_location must be an instance of RealLocation')
        self._real_location = v
        return self

    def get_real_orientation(self):
        return self._real_orientation
    def unset_real_orientation(self):
        self._real_orientation = None
        return self
    def set_real_orientation(self, v):
        if not isinstance(v, RealOrientation):
            raise TypeError('arguments to set_real_orientation must be an instance of RealOrientation')
        self._real_orientation = v
        return self

    def get_uuid(self):
        return self._uuid
    def unset_uuid(self):
        self._uuid = None
        return self
    def set_uuid(self, v):
        if not isinstance(v, Uuid):
            raise TypeError('arguments to set_uuid must be an instance of Uuid')
        self._uuid = v
        return self

