class Name:
    def __init__(self, name):
        if not isinstance(name, str):
            raise TypeError("climb name is not a string")
        if name == "":
            raise ValueError("climb name is an empty string")
        self._name = name
    @staticmethod
    def from_data(j):
        return Name(j)
    def to_data(self):
        return self._name

class AlternateNames:
    def __init__(self, names):
        if not isinstance(names, list):
            raise TypeError("climb alternate names is not a list")
        self._names = []
        for n in names:
            self._names.append(Name(n))
    @staticmethod
    def from_data(j):
        return AlternateNames(j)
    def to_data(self):
        return [n.to_data() for n in self._names]

class Rating:
    def __init__(self, difficulty=None, protection=None, style=None):
        self._difficulty = None
        self._protection = None
        self._style = None
        if not (difficulty or protection or style):
            raise ValueError('climb.rating must not be empty.' +
                             'it must contain a combo of difficulty, protection, and style.')
        if difficulty is not None:
            if not isinstance(difficulty, str):
                raise TypeError('climb.rating.difficulty must not be a string')
            if difficulty == '':
                raise TypeError('climb.rating.difficulty must not be empty')
            else:
                self._difficulty = difficulty
        if protection is not None:
            if not isinstance(protection, str):
                raise TypeError('climb.rating.protection must not be a string')
            if protection == '':
                raise TypeError('climb.rating.protection must not be empty')
            else:
                self._protection = protection
        if style is not None:
            if not isinstance(style, str):
                raise TypeError('climb.rating.style must not be a string')
            if style == '':
                raise TypeError('climb.rating.style must not be empty')
            else:
                self._style = style
    @staticmethod
    def from_data(j):
        kwargs = {}
        if not isinstance(j, dict):
            raise TypeError('climb.rating data must be a dict')
        for k, v in j.items():
            if k == 'difficulty':
                kwargs['difficulty'] = v
            elif k == 'protection':
                kwargs['protection'] = v
            elif k == 'style':
                kwargs['style'] = v
            else:
                raise ValueError(f'unexpected key in climb.rating data: {k}')
        return Rating(**kwargs)
    def to_data(self):
        j = {}
        if self._difficulty: j['difficulty'] = self._difficulty
        if self._protection: j['protection'] = self._protection
        if self._style:      j['style']      = self._style
        return j

class Length:
    def __init__(self, lower=None, upper=None, unit=None):
        if not isinstance(lower, (int, float)):
            raise TypeError("climb length's lower estimate must be a number")
        if lower <= 0:
            raise ValueError("climb length's lower estimate must be greater than 0")
        if not isinstance(upper, (int, float)):
            raise TypeError("climb length's upper estimate must be a number")
        if upper <= 0:
            raise ValueError("climb length's upper estimate must be greater than 0")
        if upper < lower:
            raise ValueError("climb length's upper estimate can't be smaller than its lower estimate")
        if not isinstance(unit, str):
            raise TypeError("climb length's unit must be a string")
        if unit == "":
            raise ValueError("climb length's unit must not be empty")
        self._lower = lower
        self._upper = upper
        self._unit = unit
    @staticmethod
    def from_data(j):
        kwargs = {}
        if 'lowerEstimate' in j:
            kwargs['lower'] = j['lowerEstimate']
        if 'upperEstimate' in j:
            kwargs['upper'] = j['upperEstimate']
        if 'unit' in j:
            kwargs['unit'] = j['unit']
        return Length(**kwargs)
    def to_data(self):
        return {
            'lowerEstimate': self._lower,
            'upperEstimate': self._upper,
            'unit': self._unit,
        }

class Location:
    def __init__(self, longitude=None, latitude=None, radius=None):
        if not isinstance(longitude, (int, float)):
            raise TypeError("climb location's longitude must be a number")
        if not isinstance(latitude, (int, float)):
            raise TypeError("climb location's latitude must be a number")
        if not isinstance(radius, (int, float)):
            raise TypeError("climb location's meterRadius must be a number")
        if longitude < -180 or 180 < longitude:
            raise ValueError("climb location's longitude must be between -180 and 180")
        if latitude < -90 or 90 < latitude:
            raise ValueError("climb location's latitude must be between -90 and 90")
        if radius < 0:
            raise ValueError("climb location's meterRadius must not be negative")
        self._longitude = longitude
        self._latitude = latitude
        self._radius = radius
    @staticmethod
    def from_data(j):
        kwargs = {}
        if 'longitude' in j:
            kwargs['longitude'] = j['longitude']
        if 'latitude' in j:
            kwargs['latitude'] = j['latitude']
        if 'meterRadius' in j:
            kwargs['radius'] = j['meterRadius']
        return Location(**kwargs)
    def to_data(self):
        return {
            'longitude': self._longitude,
            'latitude': self._latitude,
            'meterRadius': self._radius,
        }

class Ascents:
    def __init__(self, ascents):
        if not isinstance(ascents, list):
            raise TypeError("climb ascents must be a list")
        self._ascents = []
        for ascent in ascents:
            if not isinstance(ascent, str):
                raise TypeError("each climb ascent must be a string")
            if ascent == "":
                raise ValueError("each climb ascent must be a non-empty string")
            self._ascents.append(ascent)
    @staticmethod
    def from_data(j):
        return Ascents(j)
    def to_data(self):
        return [n for n in self._ascents]

class ProjectStatus:
    def __init__(self, status):
        if not isinstance(status, str) and status is not None:
            raise TypeError("climb project-status must be a string or None")
        if isinstance(status, str) and status == "":
            raise ValueError("climb project-status must be a non-empty string (or None)")
        self._status = status
    @staticmethod
    def from_data(j):
        return ProjectStatus(j)
    def to_data(self):
        return self._status

class Steepness:
    def __init__(self, segments):
        if not isinstance(segments, list):
            raise TypeError("climb steepness must be a list")
        if len(segments) == 0:
            raise ValueError("climb steepness must be a non-empty list")
        a = []
        for idx, (starting_at, degrees_overhung) in enumerate(segments):
            if not isinstance(starting_at, (int, float)):
                raise TypeError("climb steepness segment's \"starting_at\" must be a number")
            if starting_at < 0 or 1 < starting_at:
                raise ValueError("climb steepness segments's \"starting_at\" must be a number from 0 to 1")
            if not isinstance(degrees_overhung, (int, float)):
                raise TypeError("climb steepness segment's \"degrees_overhung\" must be a number")
            if degrees_overhung < -180 or 180 < degrees_overhung:
                raise ValueError("climb steepness segments's \"degrees_overhung\" must be a number from -180 to 180")
            if idx == 0:
                if starting_at != 0:
                    raise ValueError("climb steepness segments's first \"starting_at\" value must be 0")
            else:
                if starting_at <= a[-1][0]:
                    raise ValueError("climb steepness segments must have increasing \"starting_at\" values")
            a.append((starting_at, degrees_overhung))
        self._segments = a
    @staticmethod
    def from_data(j):
        a = []
        for seg in j: 
            a.append((seg['startingAt'], seg['degreesOverhung']))
        return Steepness(a)
    def to_data(self):
        j = []
        for a, b in self._segments:
            j.append({
                'startingAt': a,
                'degreesOverhung': b,
            })
        return j

class Terrain:
    def __init__(self, segments):
        if not isinstance(segments, list):
            raise TypeError("climb terrain must be a list")
        if len(segments) == 0:
            raise ValueError("climb terrain must be a non-empty list")
        verified = []
        for idx, (starting_at, desc) in enumerate(segments):
            if not isinstance(starting_at, (int, float)):
                raise TypeError("climb terrain segment's \"starting_at\" must be a number")
            if starting_at < 0 or 1 < starting_at:
                raise ValueError("climb terrain segments's \"starting_at\" must be a number from 0 to 1")
            if not isinstance(desc, str):
                raise TypeError("climb terrain segment's \"description\" must be a string")
            if desc == '':
                raise ValueError("climb terrain segments's \"description\" must not be empty")
            if idx == 0:
                if starting_at != 0:
                    raise ValueError("climb terrain segments's first \"starting_at\" value must be 0")
            else:
                if starting_at <= verified[-1][0]:
                    raise ValueError("climb terrain segments must have increasing \"starting_at\" values")
            verified.append((starting_at, desc))
        self._segments = verified
    @staticmethod
    def from_data(j):
        a = []
        for seg in j: 
            a.append((seg['startingAt'], seg['description']))
        return Terrain(a)
    def to_data(self):
        j = []
        for a, b in self._segments:
            j.append({
                'startingAt': a,
                'description': b,
            })
        return j

class Tags:
    def __init__(self, tags):
        if not isinstance(tags, list):
            raise TypeError("climb tags must be a list")
        self._tags = []
        for tag in tags:
            if not isinstance(tag, str):
                raise TypeError("climb tags must be a string")
            if tag == "":
                raise ValueError("climb tag must be a non-empty string")
            self._tags.append(tag)
    @staticmethod
    def from_data(j):
        return Tags(j)
    def to_data(self):
        return [t for t in self._tags]

class Fields:
    def __init__(self, fields):
        if not isinstance(fields, dict):
            raise TypeError("climb fields must be a dict")
        self._fields = {}
        for f, v in fields.items():
            if not isinstance(v, str):
                raise TypeError("climb field values must be a string")
            self._fields[f] = v
    @staticmethod
    def from_data(j):
        return Fields(j)
    def to_data(self):
        return self._fields.copy()

class Notes:
    def __init__(self, notes):
        if not isinstance(notes, list):
            raise TypeError("climb notes must be a list")
        good = []
        topics = set()
        for (topic, content) in notes:
            if not isinstance(topic, str):
                raise TypeError("climb note topic must be a string")
            if topic == '':
                raise ValueError("climb notes topic must not be empty")
            if topic in topics:
                raise ValueError("climb notes topics must all be unique")
            if not isinstance(content, str):
                raise TypeError("climb note content must be a string")
            if topic == '':
                raise ValueError("climb notes topic must not be empty")
            topics.add(topic)
            good.append((topic, content))
        self._notes = good
    @staticmethod
    def from_data(j):
        return Notes([(n['topic'], n['content']) for n in j])
    def to_data(self):
        return [{'topic': t, 'content': c} for (t, c) in self._notes]


class Resources:
    def __init__(self, resources):
        if not isinstance(resources, list):
            raise TypeError("climb resources must be a list")
        good = []
        for (link, title, desc) in resources:
            if not isinstance(link, str):
                raise TypeError("each item in climb resources must have a resource field that is a string")
            if link == '':
                raise TypeError("each item in climb resources \"resource\" field that is a non-empty string")
            if title and not isinstance(title, str):
                raise ValueError("if defined, the climb resource title must be a string")
            if desc and not isinstance(desc, str):
                raise ValueError("if defined, the climb resource description must be a string")
            good.append((link, title, desc))
        self._resources = good
    @staticmethod
    def from_data(j):
        a = []
        for r in j:
            a.append((r.get('resource'), r.get('title'), r.get('description')))
        return Resources(a)
    def to_data(self):
        d = []
        for (link, title, desc) in self._resources:
           a = {'resource':link}
           if title: a['title'] = title
           if desc: a['description'] = desc
           d.append(a)
        return d


class Uuid:
    def __init__(self, a):
        import uuid
        if isinstance(a, str):
            self._uuid = uuid.UUID(a)
        elif isinstance(a, uuid.UUID):
            self._uuid = a
        else:
            raise TypeError("climb uuid must be a string or UUID")
    @staticmethod
    def from_data(a):
        import re
        rex = re.compile('[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}')
        if not isinstance(a, str):
            raise TypeError("climb uuid must be a string")
        if not rex.match(a):
            raise ValueError("climb uuid string must look like a uuid: " +
                             "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx")
        return Uuid(a)
    def to_data(self):
        return str(self._uuid)

class Climb:

    def __init__(self):
        self._alternate_names = None
        self._ascents = None
        self._fields = None
        self._length = None
        self._location = None
        self._name = None
        self._notes = None
        self._project_status = None
        self._rating = None
        self._resources = None
        self._steepness = None
        self._tags = None
        self._terrain = None
        self._uuid = None

    @staticmethod
    def from_data(d):
        a = Climb()
        for k, v in d.items():
            if k == 'alternateNames':
                a._alternate_names = AlternateNames.from_data(v)
            elif k == 'ascents':
                a._ascents = Ascents.from_data(v)
            elif k == 'fields':
                a._fields = Fields.from_data(v)
            elif k == 'length':
                a._length = Length.from_data(v)
            elif k == 'location':
                a._location = Location.from_data(v)
            elif k == 'name':
                a._name = Name.from_data(v)
            elif k == 'notes':
                a._notes = Notes.from_data(v)
            elif k == 'projectStatus':
                a._project_status = ProjectStatus.from_data(v)
            elif k == 'rating':
                a._rating = Rating.from_data(v)
            elif k == 'resources':
                a._resources = Resources.from_data(v)
            elif k == 'steepness':
                a._steepness = Steepness.from_data(v)
            elif k == 'tags':
                a._tags = Tags.from_data(v)
            elif k == 'terrain':
                a._terrain = Terrain.from_data(v)
            elif k == 'uuid':
                a._uuid = Uuid.from_data(v)
            else:
                raise ValueError('unexpected key ' + k)
        return a

    def to_data(self):
        d = {}
        if self._alternate_names:
            d['alternateNames'] = self._alternate_names.to_data()
        if self._ascents:
            d['ascents'] = self._ascents.to_data()
        if self._fields:
            d['fields'] = self._fields.to_data()
        if self._length:
            d['length'] = self._length.to_data()
        if self._location:
            d['location'] = self._location.to_data()
        if self._name:
            d['name'] = self._name.to_data()
        if self._notes:
            d['notes'] = self._notes.to_data()
        if self._project_status:
            d['projectStatus'] = self._project_status.to_data()
        if self._rating:
            d['rating'] = self._rating.to_data()
        if self._resources:
            d['resources'] = self._resources.to_data()
        if self._steepness:
            d['steepness'] = self._steepness.to_data()
        if self._tags:
            d['tags'] = self._tags.to_data()
        if self._terrain:
            d['terrain'] = self._terrain.to_data()
        if self._uuid:
            d['uuid'] = self._uuid.to_data()
        return d

    def get_alternate_names(self):
        return self._alternate_names
    def unset_alternate_names(self):
        self._alternate_names = None
        return self
    def set_alternate_names(self, v):
        if not isinstance(v, AlternateNames):
            raise TypeError('arguments to set_alternate_names must be an instance of AlternateNames')
        self._alternate_names = v
        return self

    def get_ascents(self):
        return self._ascents
    def unset_ascents(self):
        self._ascents = None
        return self
    def set_ascents(self, v):
        if not isinstance(v, Ascents):
            raise TypeError('arguments to set_ascents must be an instance of Ascents')
        self._ascents = v
        return self

    def get_fields(self):
        return self._fields
    def unset_fields(self):
        self._fields = None
        return self
    def set_fields(self, v):
        if not isinstance(v, Fields):
            raise TypeError('arguments to set_fields must be an instance of Fields')
        self._fields = v
        return self

    def get_length(self):
        return self._length
    def unset_length(self):
        self._length = None
        return self
    def set_length(self, v):
        if not isinstance(v, Length):
            raise TypeError('arguments to set_length must be an instance of Length')
        self._length = v
        return self

    def get_location(self):
        return self._location
    def unset_location(self):
        self._location = None
        return self
    def set_location(self, v):
        if not isinstance(v, Location):
            raise TypeError('arguments to set_location must be an instance of Location')
        self._location = v
        return self

    def get_name(self):
        return self._name
    def unset_name(self):
        self._name = None
        return self
    def set_name(self, v):
        if not isinstance(v, Name):
            raise TypeError('arguments to set_name must be an instance of Name')
        self._name = v
        return self

    def get_notes(self):
        return self._notes
    def unset_notes(self):
        self._notes = None
        return self
    def set_notes(self, v):
        if not isinstance(v, Notes):
            raise TypeError('arguments to set_notes must be an instance of Notes')
        self._notes = v
        return self

    def get_project_status(self):
        return self._project_status
    def unset_project_status(self):
        self._project_status = None
        return self
    def set_project_status(self, v):
        if not isinstance(v, ProjectStatus):
            raise TypeError('arguments to set_project_status must be an instance of ProjectStatus')
        self._project_status = v
        return self

    def get_rating(self):
        return self._rating
    def unset_rating(self):
        self._rating = None
        return self
    def set_rating(self, v):
        if not isinstance(v, Rating):
            raise TypeError('arguments to set_rating must be an instance of Rating')
        self._rating = v
        return self

    def get_resources(self):
        return self._resources
    def unset_resources(self):
        self._resources = None
        return self
    def set_resources(self, v):
        if not isinstance(v, Resources):
            raise TypeError('arguments to set_resources must be an instance of Resources')
        self._resources = v
        return self

    def get_steepness(self):
        return self._steepness
    def unset_steepness(self):
        self._steepness = None
        return self
    def set_steepness(self, v):
        if not isinstance(v, Steepness):
            raise TypeError('arguments to set_steepness must be an instance of Steepness')
        self._steepness = v
        return self

    def get_tags(self):
        return self._tags
    def unset_tags(self):
        self._tags = None
        return self
    def set_tags(self, v):
        if not isinstance(v, Tags):
            raise TypeError('arguments to set_tags must be an instance of Tags')
        self._tags = v
        return self

    def get_terrain(self):
        return self._terrain
    def unset_terrain(self):
        self._terrain = None
        return self
    def set_terrain(self, v):
        if not isinstance(v, Terrain):
            raise TypeError('arguments to set_terrain must be an instance of Terrain')
        self._terrain = v
        return self

    def get_uuid(self):
        return self._uuid
    def unset_uuid(self):
        self._uuid = None
        return self
    def set_uuid(self, v):
        if not isinstance(v, Uuid):
            raise TypeError('arguments to set_uuid must be an instance of Uuid')
        self._uuid = v
        return self

