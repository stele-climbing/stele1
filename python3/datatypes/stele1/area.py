class Name:
    def __init__(self, name):
        if not isinstance(name, str):
            raise TypeError("area name is not a string")
        if name == "":
            raise ValueError("area name is an empty string")
        self._name = name
    @staticmethod
    def from_data(j):
        return Name(j)
    def to_data(self):
        return self._name

class AlternateNames:
    def __init__(self, names):
        if not isinstance(names, list):
            raise TypeError("area alternate names is not a list")
        self._names = []
        for n in names:
            self._names.append(Name(n))
    @staticmethod
    def from_data(j):
        return AlternateNames(j)
    def to_data(self):
        return [n.to_data() for n in self._names]

class Location:
    # TODO: use these internally
    # APPROXIMATION = 'approximation' 
    # PERIMETER = 'perimeter'
    def __init__(self, type, coords):
        if not isinstance(type, str):
            raise TypeError("area location type must be a string")
        if type == 'approximation':
            self._type = 'approximation'
            if not isinstance(coords, dict):
                raise TypeError("an \"approximation\" area location coordinates must be a dict")
            # TODO: check structure
            if not isinstance(coords['latitude'], (int, float)):
                raise TypeError("area location's latitude must be numbers")
            if coords['latitude'] < -90 or 90 < coords['latitude']:
                raise TypeError("area location's latitude must from -90 to 90")
            if not isinstance(coords['longitude'], (int, float)):
                raise TypeError("area location's longitude must be numbers")
            if coords['longitude'] < -180 or 180 < coords['longitude']:
                raise TypeError("area location's longitude must from -180 to 180")
            if not isinstance(coords['meterRadius'], (int, float)):
                raise TypeError("area location's meterRadius must be a number")
            if coords['meterRadius'] <= 0:
                raise TypeError("area location's meterRadius must be greater than 0")
            self._coords = {
                'longitude': coords['longitude'],
                'latitude': coords['latitude'],
                'radius': coords['meterRadius'],
            }
        elif type == 'perimeter':
            self._type = 'perimeter'
            self._coords = []
            if not isinstance(coords, list):
                raise TypeError("a \"perimeter\" area location's coordinates must be a list")
            if not isinstance(coords[0], dict):
                raise ValueError("a \"perimeter\" area location must be an array of {'longitude': lng, 'latitude': lat} points")
            if len(coords) < 4:
                raise ValueError("a \"perimeter\" area location must have at least 4 points")
            for ll in coords:
                if not isinstance(ll['latitude'], (int, float)):
                    raise TypeError("area location's latitude must be numbers")
                if ll['latitude'] < -90 or 90 < ll['latitude']:
                    raise TypeError("area location's latitude must from -90 to 90")
                if not isinstance(ll['longitude'], (int, float)):
                    raise TypeError("area location's longitude must be numbers")
                if ll['longitude'] < -180 or 180 < ll['longitude']:
                    raise TypeError("area location's longitude must from -180 to 180")
                self._coords.append((ll['longitude'], ll['latitude']))
        else:
            raise ValueError("area location type must be a \"approximation\" or \"perimeter\"")
    @staticmethod
    def approximation(lngLat, meterRadius):
        longitude, latitude = lngLat
        return Location('approximation', {
            'longitude': longitude,
            'latitude': latitude,
            'meterRadius': meterRadius,
        })
    @staticmethod
    def perimeter(pts):
        return Location('perimeter', [{
            'latitude': lat,
            'longitude': lng
        } for (lng, lat) in pts])
    @staticmethod
    def from_data(j):
        if j['type'] == 'approximation':
            return Location('approximation', j['circle'])
        if j['type'] == 'perimeter':
            return Location('perimeter', j['polygon'])
    def to_data(self):
        if self._type == 'approximation':
            return {
                'type': self._type,
                'circle': {
                    'longitude': self._coords['longitude'],
                    'latitude': self._coords['latitude'],
                    'meterRadius': self._coords['radius'],
                },
            }
        if self._type == 'perimeter':
            return {
                'type': self._type,
                'polygon': [{'latitude': lat, 'longitude': lng} for (lng, lat) in self._coords],
            }

class Tags:
    def __init__(self, tags):
        if not isinstance(tags, list):
            raise TypeError("area tags must be a list")
        self._tags = []
        for tag in tags:
            if not isinstance(tag, str):
                raise TypeError("area tags must be a string")
            if tag == "":
                raise ValueError("area tag must be a non-empty string")
            self._tags.append(tag)
    @staticmethod
    def from_data(j):
        return Tags(j)
    def to_data(self):
        return [t for t in self._tags]

class Fields:
    def __init__(self, fields):
        if not isinstance(fields, dict):
            raise TypeError("area fields must be a dict")
        self._fields = {}
        for f, v in fields.items():
            if not isinstance(v, str):
                raise TypeError("area field values must be a string")
            self._fields[f] = v
    @staticmethod
    def from_data(j):
        return Fields(j)
    def to_data(self):
        return self._fields.copy()

class Notes:
    def __init__(self, notes):
        if not isinstance(notes, list):
            raise TypeError("area notes must be a list")
        good = []
        topics = set()
        for (topic, content) in notes:
            if not isinstance(topic, str):
                raise TypeError("area note topic must be a string")
            if topic == '':
                raise ValueError("area notes topic must not be empty")
            if topic in topics:
                raise ValueError("area notes topics must all be unique")
            if not isinstance(content, str):
                raise TypeError("area note topic must be a string")
            if topic == '':
                raise ValueError("area notes topic must not be empty")
            topics.add(topic)
            good.append((topic, content))
        self._notes = good
    @staticmethod
    def from_data(j):
        return Notes([(n['topic'], n['content']) for n in j])
    def to_data(self):
        return [{'topic': t, 'content': c} for (t, c) in self._notes]

class Uuid:
    def __init__(self, a):
        import uuid
        if isinstance(a, str):
            self._uuid = uuid.UUID(a)
        elif isinstance(a, uuid.UUID):
            self._uuid = a
        else:
            raise TypeError("area uuid must be a string or UUID")
    @staticmethod
    def from_data(a):
        import re
        rex = re.compile('[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}')
        if not isinstance(a, str):
            raise TypeError("area uuid must be a string")
        if not rex.match(a):
            raise ValueError("area uuid string must look like a uuid: " +
                             "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx")
        return Uuid(a)
    def to_data(self):
        return str(self._uuid)

class Area:

    def __init__(self):
        self._alternate_names = None
        self._fields = None
        self._location = None
        self._name = None
        self._notes = None
        self._tags = None
        self._uuid = None

    @staticmethod
    def from_data(d):
        a = Area()
        for k, v in d.items():
            if k == 'alternateNames':
                a._alternate_names = AlternateNames.from_data(v)
            elif k == 'fields':
                a._fields = Fields.from_data(v)
            elif k == 'location':
                a._location = Location.from_data(v)
            elif k == 'name':
                a._name = Name.from_data(v)
            elif k == 'notes':
                a._notes = Notes.from_data(v)
            elif k == 'tags':
                a._tags = Tags.from_data(v)
            elif k == 'uuid':
                a._uuid = Uuid.from_data(v)
            else:
                raise ValueError('unexpected key ' + k)
        return a

    def to_data(self):
        d = {}
        if self._alternate_names:
            d['alternateNames'] = self._alternate_names.to_data()
        if self._fields:
            d['fields'] = self._fields.to_data()
        if self._location:
            d['location'] = self._location.to_data()
        if self._name:
            d['name'] = self._name.to_data()
        if self._notes:
            d['notes'] = self._notes.to_data()
        if self._tags:
            d['tags'] = self._tags.to_data()
        if self._uuid:
            d['uuid'] = self._uuid.to_data()
        return d

    def get_alternate_names(self):
        return self._alternate_names
    def unset_alternate_names(self):
        self._alternate_names = None
        return self
    def set_alternate_names(self, v):
        if not isinstance(v, AlternateNames):
            raise TypeError('arguments to set_alternate_names must be an instance of AlternateNames')
        self._alternate_names = v
        return self

    def get_fields(self):
        return self._fields
    def unset_fields(self):
        self._fields = None
        return self
    def set_fields(self, v):
        if not isinstance(v, Fields):
            raise TypeError('arguments to set_fields must be an instance of Fields')
        self._fields = v
        return self

    def get_location(self):
        return self._location
    def unset_location(self):
        self._location = None
        return self
    def set_location(self, v):
        if not isinstance(v, Location):
            raise TypeError('arguments to set_location must be an instance of Location')
        self._location = v
        return self

    def get_name(self):
        return self._name
    def unset_name(self):
        self._name = None
        return self
    def set_name(self, v):
        if not isinstance(v, Name):
            raise TypeError('arguments to set_name must be an instance of Name')
        self._name = v
        return self

    def get_notes(self):
        return self._notes
    def unset_notes(self):
        self._notes = None
        return self
    def set_notes(self, v):
        if not isinstance(v, Notes):
            raise TypeError('arguments to set_notes must be an instance of Notes')
        self._notes = v
        return self

    def get_tags(self):
        return self._tags
    def unset_tags(self):
        self._tags = None
        return self
    def set_tags(self, v):
        if not isinstance(v, Tags):
            raise TypeError('arguments to set_tags must be an instance of Tags')
        self._tags = v
        return self

    def get_uuid(self):
        return self._uuid
    def unset_uuid(self):
        self._uuid = None
        return self
    def set_uuid(self, v):
        if not isinstance(v, Uuid):
            raise TypeError('arguments to set_uuid must be an instance of Uuid')
        self._uuid = v
        return self

