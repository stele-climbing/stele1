class Name:
    def __init__(self, name):
        if not isinstance(name, str):
            raise TypeError("metadata name must be a string")
        if name == "":
            raise ValueError("metadata name must be a non-empty string")
        self._name = name
    @staticmethod
    def from_data(j):
        return Name(j)
    def to_data(self):
        return self._name

class Description:
    def __init__(self, description):
        if not isinstance(description, str):
            raise TypeError("metadata description must be a string")
        if description == "":
            raise ValueError("metadata description must be a non-empty string")
        self._description = description
    @staticmethod
    def from_data(j):
        return Description(j)
    def to_data(self):
        return self._description

class Authors:
    def __init__(self, authors):
        if not isinstance(authors, list):
            raise TypeError("metadata authors must be a list")
        self._authors = []
        for author in authors:
            if not isinstance(author, str):
                raise TypeError("each metadata author must be a string")
            if author == "":
                raise ValueError("each metadata author must be a non-empty string")
            self._authors.append(author)
    @staticmethod
    def from_data(j):
        return Authors(j)
    def to_data(self):
        return [n for n in self._authors]

class Uuid:
    def __init__(self, a):
        import uuid
        if isinstance(a, str):
            self._uuid = uuid.UUID(a)
        elif isinstance(a, uuid.UUID):
            self._uuid = a
        else:
            raise TypeError("metadata uuid must be a string or UUID")
    @staticmethod
    def from_data(a):
        import re
        rex = re.compile('[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}')
        if not isinstance(a, str):
            raise TypeError("metadata uuid must be a string")
        if not rex.match(a):
            raise ValueError("metadata uuid string must look like a uuid: " +
                             "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx")
        return Uuid(a)
    def to_data(self):
        return str(self._uuid)

class Metadata:

    def __init__(self):
        self._authors = None
        self._description = None
        self._name = None
        self._uuid = None

    @staticmethod
    def from_data(d):
        a = Metadata()
        for k, v in d.items():
            if k == 'authors':
                a._authors = Authors.from_data(v)
            elif k == 'description':
                a._description = Description.from_data(v)
            elif k == 'name':
                a._name = Name.from_data(v)
            elif k == 'uuid':
                a._uuid = Uuid.from_data(v)
            else:
                raise ValueError('unexpected key ' + k)
        return a

    def to_data(self):
        d = {}
        if self._authors:
            d['authors'] = self._authors.to_data()
        if self._description:
            d['description'] = self._description.to_data()
        if self._name:
            d['name'] = self._name.to_data()
        if self._uuid:
            d['uuid'] = self._uuid.to_data()
        return d

    def get_authors(self):
        return self._authors
    def unset_authors(self):
        self._authors = None
        return self
    def set_authors(self, v):
        if not isinstance(v, Authors):
            raise TypeError('arguments to set_authors must be an instance of Authors')
        self._authors = v
        return self

    def get_description(self):
        return self._description
    def unset_description(self):
        self._description = None
        return self
    def set_description(self, v):
        if not isinstance(v, Description):
            raise TypeError('arguments to set_description must be an instance of Description')
        self._description = v
        return self

    def get_name(self):
        return self._name
    def unset_name(self):
        self._name = None
        return self
    def set_name(self, v):
        if not isinstance(v, Name):
            raise TypeError('arguments to set_name must be an instance of Name')
        self._name = v
        return self

    def get_uuid(self):
        return self._uuid
    def unset_uuid(self):
        self._uuid = None
        return self
    def set_uuid(self, v):
        if not isinstance(v, Uuid):
            raise TypeError('arguments to set_uuid must be an instance of Uuid')
        self._uuid = v
        return self

