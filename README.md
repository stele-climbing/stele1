# stele1

stele1 is for rock climbing data.
It provides a straightforward, stable base
for managing climbs, areas, photos, and other guidebook contents.

Whenever possible, stele1 leans on well-known, existing standards.
Generally, stele1 uses JSON.
For some data, stele1 allows blobs (e.g., for images).

On the filesystem, stele1 prescribes a transparent layout for this data.

stele1 will continue to be stable and straightforward.

## Goals

- Obvious
- Build on simple tools
- Don't be fancy
  - if fanciness lives in libraries, other climbing software can use it
    - interpret and compare grades programatically
    - build a gpx file

<!-- TODO: add stuff about the spec/storage/usage/... -->
