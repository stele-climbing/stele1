## scripts

run tests:

```sh
$ VERBOSE=y ./misc/run-generated-datatype-tests
```

## next steps

- https://archive.org/services/docs/api/metadata-schema/index.html#external-identifier
- https://archive.org/services/docs/api/metadata-schema/index.html#identifier-ark
- protection/safety/commitment
  - what is the right term for what we are trying to describe?
    - does "G" mean Protection Abundant? The climb is safe? most falls are safe? most _likely_ falls are safe? low consequences?
- rename "ascents" to "events"
  - this can include route developer
  - this gives climbs personality, and hightlights that it's shared by the community
  - https://www.mountainproject.com/forum/topic/116157988/why-fas-dont-matter
  - Proper Soul:
    -> "1989 [Started Equipping] Porter Jarrard" https://web.archive.org/web/20150625032248/http://www.dpmclimbing.com/articles/view/porter-jarrard-climbing-514-44
    -> "1997 Brian McCray"                       https://www.mountainproject.com/route/107337912/proper-soul
    -> "2008 [Onsight] Chris Sharma"                    
    -> "2010 Porter Jarrard"                     https://web.archive.org/web/20150625032248/http://www.dpmclimbing.com/articles/view/porter-jarrard-climbing-514-44
    -> "2010 [Trad] Brent Perkins"               https://www.ukclimbing.com/videos/categories/trad_climbing/brett_perkins_climbing_proper_soul_514a_on_gear-518
- photo fields
  - should `real{Location,DateTimeOriginal}` just be captureLocation and captureTime?
  - add `tags`
- clarify "length":
  - how to handle mantles, like the UFO Mantle (96f8b78f-7826-40d7-a087-89d8e64bdf86) in descanso.
    is it 4 feet?
    the climber-distance? how long/high it feels?
  - would "moves" be better?
- [lon lat lon lat](https://macwright.com/lonlat/)
  - [orange site](https://news.ycombinator.com/item?id=30228981)
- [GeoTIFF](https://en.m.wikipedia.org/wiki/GeoTIFF)
- should start&stop points on a polygon be duplicated?
  it makes modification awkward (but drawing easy?). 
  why do other libraries (postgis? postgres?) do it?
- projectStatus shouldn't use `null` for a non project
  - an absense of this should be sufficient
  - there is no other stele1 attribute that uses `null` as a meaningful value
- docs for people who...
  - are entirely new
  - want to use it
    - install
    - web interface
  - people who want to contribute to the ecosystem
  - people who want to contribute to core
  - myself
- how should (UU)ID be handled?
  - https://sudhir.io/uuids-ulids
- rename Project -> Product/Page/Pamphlet/Deck/Tab(let)/Binder/Notebook/Portfolio/{Umbrella,Squirrel}
- rename Metadata -> Miscellaneous
  - is there a name/term better than "metadata"?  context, document, project-info?
- verify that python/README.md instructions work a on a clean development system
- verify that javascript/README.md instructions work a on a clean development system
- autogenerate a diagram to explain schema aspects
  - Graphviz ([example](https://graphviz.gitlab.io/_pages/Gallery/undirected/ER.html))
  - [PIC](http://floppsie.comp.glam.ac.uk/Glamorgan/gaius/web/pic.html)
- add a script to autogenerate tests for the datatypes and filesystem _based on a repo_
- use a registration/composition model for entities and attributes
  - extension support isn't thought-out
  - photo and svg support should be better modeled
    - if a field is a blob it can be stored as a separate file: {uuid}_{attr-name-kebab}.ext???
- expand the spec to include <entity>__<field>__{examples.json,description.md,summary.txt,usage/practices.md}
  - usage/practices for climb note fields would establish standard note topics
    - `Pitch200` - a <=200 character elevator pitch for the climb
    - `Pitch500` - a <=500 character elevator pitch for the climb
    - `History` - broken holds?
    - `FA Story`
    - `FFA Story`
    - `Context`
    - `Controversy`
    - `Approach Details`
    - `Beta`
- doc site
  - https://www.gnu.org/software/guile/
  - https://cons.io/
  - https://racket-lang.org/
  - include something interactive
- publish {js,py} packages
  - egg vs wheel? https://pythonwheels.com/
  - python module naming conventions 
    - PEP
      - [`PEP 0008`](https://www.python.org/dev/peps/pep-0008/#package-and-module-names)
      - [`PEP 0423`](https://www.python.org/dev/peps/pep-0423/)
    - [packaging namespace packages](https://packaging.python.org/guides/packaging-namespace-packages/#creating-a-namespace-package)
    - options
      - `stele1_{datatypes,filesystem}`
      - `stele1.{datatypes,filesystem}`
        as [hinted to by django.contrib](https://www.python.org/dev/peps/pep-0423/#respect-ownership)
      - `stele1{.,_,}{datatypes,filesystem}`
        as hinted to as django.contrib
      - `{datatypes,filesystem}stele1`
        as seen on srht
- POSIX sh stuff isn't complete; only project_create and project_exists are written
  - there's a CLI tool similar to jq that's used for building JSON objects
- publish/upload projects to the internet archive
  - https://help.archive.org/hc/en-us/articles/360002360111-Uploading-A-Basic-Guide
  - https://help.archive.org/hc/en-us/articles/360014487651-Files-Formats-and-Derivatives-A-Basic-Guide

## ecosystem-dependent

- keep an eye on https://github.com/WICG/uuid

## rough plans and half-baked ideas

- support routes with incomplete bolting/cleaning
  to make the guide useful to publishers for areas that are actively being developed
- what's an open license for the data model? is that necessary?

## breaking changes

- perhaps the canonical names should be snake_case?

## project integrity checks

- are there dangling references?
- are there duplicate names?
- tag taxonomy?
- label taxonomy?
- what climbs are not drawn on anything?
- what climbs should be in an overhead-sketch that aren't?
- what climbs should be in a photo that aren't?
- what climbs should be in a face-sketch that aren't?
- is JSON and the filesystem well formed?
  - extra files?
  - extra fields?
- are ids and filenames in sync?
- check for the same units on climb length, the same grading systems for climbs, ...
- warn when an `climb.approximate-location` is over a given threshold
- ascent/project-status
  - when nothing in climb.ascents, and nothing in climb.project-status
- will deleting an item (and associated annotations) leave some other item dangling?
  (e.g., will deleting a photo leave a climb without drawings?)

## images & diagrams

- inkscape plugin for svg
  - "live path effects" might be the tool for drawing ticks for roofs and corners
  - https://gitlab.com/inkscape/extensions
  - https://inkscape.org/develop/extensions/
  - https://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#Extensions
  - https://wiki.inkscape.org/wiki/index.php/Release_notes/1.0#For_extension_writers
- annotate raster images?
- annotate pdf?
- python establish HEIC/HEIF support
  (see [Pillow issue#2806](https://github.com/python-pillow/Pillow/issues/2806))

### preserving exif dates

from [StackExchange Photography](https://photo.stackexchange.com/questions/69959/when-is-each-of-these-exif-date-time-variables-created-and-in-what-circumstan) 

> ... Date/Time Original and Create Date should be the same for a modern camera picture.
> But if you were, for example, scanning old pictures, slides, or negatives,
> the Create Date should be the date/time you saved it to the file and
> then you would alter the Date/Time Original to the correct time, if you knew it.
> Obviously, few people are going to know the exact time a picture was taken and probably not going to bother.

## offline use

- tool to pull local copies of links in a stele1 project
- https://archive.org/about/offline-archive/
  - https://github.com/internetarchive/dweb-mirror
- map tiles?
 - check how the [RACHEL module](http://oer2go.org/viewmod/en-worldmap-10) handles it.
 - https://github.com/robertomlsoares/leaflet-offline
 - https://github.com/mWater/offline-leaflet-map
- archiving links
  - [The Wayback Machine](https://web.archive.org/)
    [on wikipedia](https://en.wikipedia.org/wiki/Wayback_Machine)
    by [the Internet Archive](https://en.wikipedia.org/wiki/Internet_Archive)
  - [archive.today](https://archive.today/)
    [on wikipedia](https://en.wikipedia.org/wiki/Archive.today)
  - [Memento archiving](https://en.wikipedia.org/wiki/Memento_Project)

## geospatial

- GeoJSON and WKT
  - [OpenLayers vector formats](http://dev.openlayers.org/examples/vector-formats.html)
  - [OpenLayers modify feature](http://dev.openlayers.org/examples/modify-feature.html)
- [GeoJSON](https://tools.ietf.org/html/rfc7946)
  - [geojson.io](http://geojson.io/#map=2/20.0/0.0)
  - [GeoJSONLint](http://geojsonlint.com/)
  - [GeoJSON viewer](http://jansanchez.com/geojson-viewer/)
- WKT
  - [Wicket](http://arthur-e.github.io/Wicket/sandbox-gmaps3.html)
  - [OpenStreetMap WKT Playground](https://github.com/clydedacruz/openstreetmap-wkt-playground)
- [use USGS data](https://github.com/kapadia/usgs)
- https://gis.stackexchange.com/questions/115433/online-wkt-and-geojson-viewer
- [WKT](https://en.wikipedia.org/wiki/Well-known_text_representation_of_geometry) geometry
  - points should be be pairs (lng, lat) instead of {latitude, longitude} objects/dicts?
    - this goes against the "absolute transparency" guideline for development
  - https://www.ogc.org/standards/sfa
- buffering geospatial objects
  - https://stackoverflow.com/questions/639695/how-to-convert-latitude-or-longitude-to-meters#11172685
  - https://en.wikipedia.org/wiki/Geographic_coordinate_system
    - https://en.wikipedia.org/wiki/Geographic_coordinate_system#Expressing_latitude_and_longitude_as_linear_units
  - https://gis.stackexchange.com/questions/75528/understanding-terms-in-length-of-degree-formula/75535#75535
- include relevant geospatial operations in the python db
  - https://qgis.org/pyqgis/3.10/core/QgsPolygon.html?highlight=polygon#qgis.core.QgsPolygon.pointDistanceToBoundary

## ratings

- [Wikipedia](https://en.wikipedia.org/)
  use wikipedia's tables for rating conversions/validation
  - pros: support wikipedia
  - pros: support self sufficiency

## unix

make the cli composable with other tools

- [`pick`](https://github.com/mptre/pick)
- `feh` or `sxiv`
- unwritten drawing tool
- unwritten gis tool
  - [Command-Line Cartography](https://medium.com/@mbostock/command-line-cartography-part-1-897aa8f8ca2c)
    could offer leads
  - [Arch Wiki's suggestions](https://wiki.archlinux.org/index.php/List_of_applications/Science#Earth_science)
  - [gdal python bindings](https://gdal.org/python/index.html)
- [ogr2ogr](https://gdal.org/programs/ogr2ogr.html)

## unsolved issues

- using a better json pretty-printer 
  to make git diffs more friendly
  - pretty print coordinate lists on a single line
    - https://github.com/python/cpython/blob/3.8/Lib/json/encoder.py
  - sort keys so they don't reorder on read/write

## 3d models

- [the Wayback Boulder](https://sketchfab.com/3d-models/wayback-f61f042851b74f50aceb911b417ec189)
  in patpasco by Dominic Albanese
  > I used Agisoft PhotoScan for this, but am also using Reality Capture lately.
  > Cleaned up in ZBrush.
  > I am an avid rock climber so I have a huge interest in scanning rocks that I come across!
- the [Black Boulder](https://sketchfab.com/3d-models/black-boulder-0b9d3b3fef024991ab529d8375266aff)
  in The Gunks by Dominic Albanese
- https://climbassist.com/
  - https://climbassist.com/crags/bell-buttress 
- there's a 3d model of woodstock rock in patapsco somewhere

## future features

- add a stele1-git-init script to
  - make sure all directories are tracked?
- types of visualizations/sketches/drawings
  - svg               -- flexible and builds on existing standards
  - overhead-sketches -- precise and could support custom rendering tools
  - face-sketches     -- precise and could support custom rendering tools
- multipitch grades? 
  - break ratings down into pitches?
  - draw pitches on scenes (photos, face-sketches)
- alternate grades?  boulder & route grades for a single 20' climb

## notes

## type and style, discipline, ethics, ...

saying "type" is generic.
type can be "trad", "sport", "psicobloc", "boulder", "crack", "big wall", "free solo", "barefoot", "redpoint", ....
few of these are mutually exclusive.

- some mutually exclusive categories:
  - **how long the climb is**:
    big wall, route, boulder
  - **how the climber protects the ascent**:
    trad, sport, top-rope, free-solo, pads (boulder),
  - **what the climbing is like**:
    crack, face, offwidth, chimney, crimpy

## simple tree for organizing/nesting areas

- seneca
  - southern pillar
  - south peak 
    - spwf
    - spef
    - south end
    - <the burn, sunshine, ...>
  - north peak
    - npwf
    - npef
  - lower slabs

```
     seneca
       /\ \
      /  \ \
     /    \ \
    /      \ \
southern    \ \
 pillar      \ \
              \  north peak
               \
                 south peak
```

## notes on naming

- [Retro-naming vs. Given Names](https://www.mountainproject.com/forum/topic/106584310/retro-naming-vs-given-names)
  - "20 years from now your problems will be re-discovered and re-named by an entirely different generation. Just the nature of bouldering." 
  - "I think a lot of energy has been wasted over something really silly like the name of a set of holds on a random block in a talus field.
     A name is only useful as a consensus description.
     There is no inalienable naming right in the consitution.
     Plenty of routes go by multiple names, and you can call it whatever you want.
     Climbing history is full of routes thate were re-named after the first ascent: Supercrack, Astroman just to name two." 
  - example of ["Spragueasorus Boulder (aka Underworld Boulder)"](https://www.mountainproject.com/area/105961471/spragueasorus-boulder-aka-underworld-boulder)
- [naming for pragmatism](https://www.mountainproject.com/forum/topic/115415047/re-name-vantage-exit-143)
  - "[...] noticed that most Seattle climbing areas are named after the highway exit that you use to access them" 

## restrictions on activity

- no access ever
- no access in nesting season
- no access in hunting season
- ethics/practice is to _not_ top-out for ecological reasons
- nursery status for areas under-development

## extensions for ammenities

- campground
- guidebooks
- grocery stores
- restaurants

## backends.txt

- unix
- browser/js
- postgresql & postgis
- sqlite/spatialite

## targets.txt

- static html
- data
- spa
- data + spa bundle
  - like google takeout:
    create a tar (more realistically zip) archive that can be opened as a file:///
    in the browser.
- pdf
- book
- openapi

<!-- TODO: clone info -->
<!-- TODO: stability info -->
<!-- TODO: branching/tagging scheme info -->
<!-- TODO: explain the directory structure -->

## spec development

<!-- TODO: explain the role of the spec -->
<!-- TODO: explain the spec directories, how they're structured -->
<!-- TODO: explain how the spec is changed -->
<!-- TODO: explain spec stability -->
<!-- TODO: explain what the spec enforces -->
