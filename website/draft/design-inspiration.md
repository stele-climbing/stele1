- [GitHub - streetcomplete/StreetComplete: Easy to use OpenStreetMap editor for Android](https://github.com/streetcomplete/StreetComplete)
- [Free as in Climbing: Open Data Project Threatened by Bogus Copyright Claims | Hacker News](https://news.ycombinator.com/item?id=26609945)
- [How I self-published a professional paperback and eBook using LaTeX and Pandoc | The Road Chose Me](http://theroadchoseme.com/how-i-self-published-a-professional-paperback-and-ebook-using-latex-and-pandoc?1#chap4)
- [Archiveteam](https://wiki.archiveteam.org/)
- [Why Lichess will always be free.](https://lichess.org/blog/YF-ZORQAACAA89PI/why-lichess-will-always-be-free.)
- [Screenshots](https://alex-hhh.github.io/2021/09/screenshots.html)
- small images
  - [Low Bandwidth Images | Opinionated Guides](https://opguides.info/posts/lowbandwidthimages/)
  - [GitHub - marceloprates/prettymaps: A small set of Python functions to draw pretty maps from OpenStreetMap data. Based on osmnx, matplotlib and shapely libraries.](https://github.com/marceloprates/prettymaps)
  - [Why Your Website Should Use Dithered Images (2020) | Lobsters](https://lobste.rs/s/cxxfyx/why_your_website_should_use_dithered)
  - [Atkinson Dithering](https://beyondloom.com/blog/dither.html)
- [Oilslick](http://mrgris.com/projects/oilslick/#) a color elevation map layer designed to highlight the fine detail in terrain,

### durable data

- pdf 
  - [Programming Language Reminiscence](https://okmij.org/ftp/Babel/index.html#postscript)
  - [Print Friendly & PDF](https://www.printfriendly.com/)
- [Lon Lat Lon Lat | Hacker News](https://news.ycombinator.com/item?id=30228981)
- [Understanding UUIDs, ULIDs and String Representations](https://sudhir.io/uuids-ulids)
- [Low Bandwidth Images](https://opguides.info/posts/lowbandwidthimages/)
- [systemfontstack](https://systemfontstack.com/)

### archiving 

- [ArchiveBox | Open source self-hosted web archiving](https://archivebox.io/)
- [Reading from the web offline](https://blog.owulveryck.info/2021/10/07/reading-from-the-web-offline-and-distraction-free.html)

### design

- single-file
  - Sqlite's Amalgamation
  - [Files – Single-file photo gallery and file manager | Hacker News](https://news.ycombinator.com/item?id=30233635)
  - [Single-file PHP file manager, browser and photo gallery | Files](https://www.files.gallery/)
  - [GitHub - Jack000/Expose: A simple static site generator for photoessays](https://github.com/Jack000/Expose)


