'use strict';

function _obj_of_keys(o, kees){
    if (typeof o !== 'object')
        return false;
    if (Object.keys(o).length !== kees.length)
        return false;
    for (var kee in o)
        if (kees.indexOf(kee) === -1)
            return false;
    return true;
}

function _obj_key_validates(o, k, f){
    return o.hasOwnProperty(k) ? f(o[k]) : false;
}

function Name(n) {
    if (typeof n !== 'string')
        throw new Error('area name is not a string');
    if (n === '')
        throw new Error('area name is an empty string');
    this._name = n;
    return this;
};
Name.fromData = function(n){ return new Name(n); };
Name.prototype.toData = function(){ return this._name; };


function Uuid(s) {
    var rex = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/;
    if (typeof s !== 'string' || !s.match(rex))
        throw new Error('area uuid must be a string that looks like a uuid');
    this._uuid = s;
    return this;
};
Uuid.fromData = function(a){ return new Uuid(a); };
Uuid.prototype.toData = function(){ return this._uuid; };


function AlternateNames(ns) {
    if (!Array.isArray(ns))
        throw new Error('area alternate-names must be an array, got ' + ns.toString());
    this._names = ns.map(function(n){ return new Name(n) });
    return this;
};
AlternateNames.fromData = function(n){ return new AlternateNames(n); };
AlternateNames.prototype.toData = function(){
   return this._names.map(function(n){ return n.toData(); });
};

function Location(typ, coords) {
    var isLat = function(l){ return typeof l === 'number' && -90 <= l && l <= 90; };
    var isLng = function(l){ return typeof l === 'number' && -180 <= l && l <= 180; };
    if (typ === "approximation") {
        if (!_obj_of_keys(coords, ["longitude", "latitude", "meterRadius"]))
            throw new Error('an "approximation" area location must have .longitude .latitude and .meterRadius');
        if (!_obj_key_validates(coords, "latitude", isLat))
            throw new Error('area location latitudes must be from -90 and 90');
        if (!_obj_key_validates(coords, "longitude", isLng))
            throw new Error('area location latitudes must be from -180 and 180');
        if (!_obj_key_validates(coords, "meterRadius", function(r){ return 0 <= r; }))
            throw new Error('area location meterRadius must not be less than 0');
        this._type = "approximation";
        this._coords = {
            latitude: coords.latitude,
            longitude: coords.longitude,
            meterRadius: coords.meterRadius,
        };
    } else if (typ === "perimeter") {
        if (!Array.isArray(coords) || coords.length < 4)
            throw new Error('a "perimeter" area location must be an array with at least 4 points');
        this._type = "perimeter";
        this._coords = [];
        for (var i in coords) {
            if (!_obj_key_validates(coords[i], "latitude", isLat))
                throw new Error('area location latitudes must be from -90 and 90');
            if (!_obj_key_validates(coords[i], "longitude", isLng))
                throw new Error('area location longitude must be from -180 and 180');
            this._coords.push({
                latitude: coords[i].latitude,
                longitude: coords[i].longitude
            });
        }
    } else {
        throw new Error('area location .type must be "approximation" or "perimeter" got ' + typ);
    }
    return this;
}
Location.fromData = function(j){
    if (j.type === 'approximation') return new Location(j.type, j.circle);
    if (j.type === 'perimeter')     return new Location(j.type, j.polygon);
};
Location.prototype.toData = function(){
   if (this._type === 'perimeter')
       return {
           type: 'perimeter',
           polygon: this._coords.map(function(pt){
               return {latitude: pt.latitude, longitude: pt.longitude};
           }),
       }
   if (this._type === 'approximation')
       return {
           type: 'approximation',
           circle: {
               longitude: this._coords.longitude,
               latitude: this._coords.latitude,
               meterRadius: this._coords.meterRadius,
           },
       }
};

function Tags(tags) {
    if (!Array.isArray(tags))
        throw new Error('area tags must be an array, got ' + tags.toString());
    var good = [];
    for (var i = 0; i < tags.length; i++) {
        if (typeof tags[i] !== 'string' || tags[i] === '')
            throw new Error('each area tag must be an a non-empty string');
        good.push(tags[i])
    }
    this._tags = good;
    return this;
};
Tags.fromData = function(n){ return new Tags(n); };
Tags.prototype.toData = function(){
   return this._tags.map(function(n){ return n; });
};

function Fields(fields) {
    if (typeof fields !== 'object')
        throw new Error('area fields must be an object');
    var good = {};
    for (var field in fields) {
        if (typeof fields[field] !== 'string')
            throw new Error('each area field value must be string');
        good[field] = fields[field];
    }
    this._fields = good;
    return this;
};
Fields.fromData = function(n){ return new Fields(n); };
Fields.prototype.toData = function(){
   var j = {};
   for (var f in this._fields) j[f] = this._fields[f];
   return j;
};

function Notes(notes) {
    if (!Array.isArray(notes))
        throw new Error('area notes must be an array');
    var good = [];
    var topics = [];
    for (var i = 0; i < notes.length; i++) {
        if (typeof notes[i].topic !== 'string' || notes[i].topic === '')
            throw new Error('all area note topics must be a non-empty string');
        if (topics.indexOf(notes[i].topic) !== -1)
            throw new Error('all area note topics be unique for a area');
        if (typeof notes[i].content !== 'string' || notes[i].content === '')
            throw new Error('all area note content must be a non-empty string');
        good.push({topic: notes[i].topic, content: notes[i].content});
    } 
    this._notes = good;
    return this;
}
Notes.fromData = function(n){ return new Notes(n); };
Notes.prototype.toData = function(){
    return this._notes.map(function(n){
        return {topic: n.topic, content: n.content};
    });
};

function Area(){
    this._alternateNames = null;
    this._fields = null;
    this._location = null;
    this._name = null;
    this._notes = null;
    this._tags = null;
    this._uuid = null;
    return this;
};
Area.prototype.toData = function(){
    let d = {};
    if (this._alternateNames)
       d.alternateNames = this._alternateNames.toData();
    if (this._fields)
       d.fields = this._fields.toData();
    if (this._location)
       d.location = this._location.toData();
    if (this._name)
       d.name = this._name.toData();
    if (this._notes)
       d.notes = this._notes.toData();
    if (this._tags)
       d.tags = this._tags.toData();
    if (this._uuid)
       d.uuid = this._uuid.toData();
    return d;
};
Area.fromData = function(d){
    let a = new Area();
    for (var k in d) {
        if (k === 'alternateNames')
            a.setAlternateNames(AlternateNames.fromData(d[k]));
        else if (k === 'fields')
            a.setFields(Fields.fromData(d[k]));
        else if (k === 'location')
            a.setLocation(Location.fromData(d[k]));
        else if (k === 'name')
            a.setName(Name.fromData(d[k]));
        else if (k === 'notes')
            a.setNotes(Notes.fromData(d[k]));
        else if (k === 'tags')
            a.setTags(Tags.fromData(d[k]));
        else if (k === 'uuid')
            a.setUuid(Uuid.fromData(d[k]));
        else
            throw new Error('unexpected key: ' + k);
    }
    return a;
};

Area.prototype.setAlternateNames = function(a){
    if (! (a instanceof AlternateNames))
        throw new Error('arguments to Area.setAlternateNames must be an instance of AlternateNames');
    this._alternateNames = a;
    return this;
};
Area.prototype.getAlternateNames = function(){
    return this._alternateNames;
};
Area.prototype.unsetAlternateNames = function(){
    this._alternateNames = null;
    return this;
};

Area.prototype.setFields = function(a){
    if (! (a instanceof Fields))
        throw new Error('arguments to Area.setFields must be an instance of Fields');
    this._fields = a;
    return this;
};
Area.prototype.getFields = function(){
    return this._fields;
};
Area.prototype.unsetFields = function(){
    this._fields = null;
    return this;
};

Area.prototype.setLocation = function(a){
    if (! (a instanceof Location))
        throw new Error('arguments to Area.setLocation must be an instance of Location');
    this._location = a;
    return this;
};
Area.prototype.getLocation = function(){
    return this._location;
};
Area.prototype.unsetLocation = function(){
    this._location = null;
    return this;
};

Area.prototype.setName = function(a){
    if (! (a instanceof Name))
        throw new Error('arguments to Area.setName must be an instance of Name');
    this._name = a;
    return this;
};
Area.prototype.getName = function(){
    return this._name;
};
Area.prototype.unsetName = function(){
    this._name = null;
    return this;
};

Area.prototype.setNotes = function(a){
    if (! (a instanceof Notes))
        throw new Error('arguments to Area.setNotes must be an instance of Notes');
    this._notes = a;
    return this;
};
Area.prototype.getNotes = function(){
    return this._notes;
};
Area.prototype.unsetNotes = function(){
    this._notes = null;
    return this;
};

Area.prototype.setTags = function(a){
    if (! (a instanceof Tags))
        throw new Error('arguments to Area.setTags must be an instance of Tags');
    this._tags = a;
    return this;
};
Area.prototype.getTags = function(){
    return this._tags;
};
Area.prototype.unsetTags = function(){
    this._tags = null;
    return this;
};

Area.prototype.setUuid = function(a){
    if (! (a instanceof Uuid))
        throw new Error('arguments to Area.setUuid must be an instance of Uuid');
    this._uuid = a;
    return this;
};
Area.prototype.getUuid = function(){
    return this._uuid;
};
Area.prototype.unsetUuid = function(){
    this._uuid = null;
    return this;
};

module.exports = {
    Area: Area,
    AlternateNames: AlternateNames,
    Fields: Fields,
    Location: Location,
    Name: Name,
    Notes: Notes,
    Tags: Tags,
    Uuid: Uuid,
};
