'use strict';


function Name(n) {
    if (typeof n !== 'string' || n === '')
        throw new Error('metadata name must be a non-empty string');
    this._name = n;
    return this;
};
Name.fromData = function(n){ return new Name(n); };
Name.prototype.toData = function(){ return this._name; };


function Uuid(s) {
    var rex = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/;
    if (typeof s !== 'string' || !s.match(rex))
        throw new Error('metadata uuid must be a string that looks like a uuid');
    this._uuid = s;
    return this;
};
Uuid.fromData = function(a){ return new Uuid(a); };
Uuid.prototype.toData = function(){ return this._uuid; };


function Description(desc) {
    if (typeof desc !== 'string' || desc === '')
        throw new Error('metadata description must be a non-empty string');
    this._description = desc;
    return this;
};
Description.fromData = function(a){ return new Description(a); };
Description.prototype.toData = function(){ return this._description; };


function Authors(authors) {
    if (!Array.isArray(authors))
        throw new Error('metadata authors must be an array');
    this._authors = [];
    for (var i in authors) {
        if (typeof authors[i] !== 'string' || authors[i] === '')
            throw new Error('each metadata author must be a non-empty string');
        this._authors.push(authors[i]);
    }
    return this;
};
Authors.fromData = function(n){ return new Authors(n); };
Authors.prototype.toData = function(){
   return this._authors.map(function(n){ return n; });
};

function Metadata(){
    this._authors = null;
    this._description = null;
    this._name = null;
    this._uuid = null;
    return this;
};
Metadata.prototype.toData = function(){
    let d = {};
    if (this._authors)
       d.authors = this._authors.toData();
    if (this._description)
       d.description = this._description.toData();
    if (this._name)
       d.name = this._name.toData();
    if (this._uuid)
       d.uuid = this._uuid.toData();
    return d;
};
Metadata.fromData = function(d){
    let a = new Metadata();
    for (var k in d) {
        if (k === 'authors')
            a.setAuthors(Authors.fromData(d[k]));
        else if (k === 'description')
            a.setDescription(Description.fromData(d[k]));
        else if (k === 'name')
            a.setName(Name.fromData(d[k]));
        else if (k === 'uuid')
            a.setUuid(Uuid.fromData(d[k]));
        else
            throw new Error('unexpected key: ' + k);
    }
    return a;
};

Metadata.prototype.setAuthors = function(a){
    if (! (a instanceof Authors))
        throw new Error('arguments to Metadata.setAuthors must be an instance of Authors');
    this._authors = a;
    return this;
};
Metadata.prototype.getAuthors = function(){
    return this._authors;
};
Metadata.prototype.unsetAuthors = function(){
    this._authors = null;
    return this;
};

Metadata.prototype.setDescription = function(a){
    if (! (a instanceof Description))
        throw new Error('arguments to Metadata.setDescription must be an instance of Description');
    this._description = a;
    return this;
};
Metadata.prototype.getDescription = function(){
    return this._description;
};
Metadata.prototype.unsetDescription = function(){
    this._description = null;
    return this;
};

Metadata.prototype.setName = function(a){
    if (! (a instanceof Name))
        throw new Error('arguments to Metadata.setName must be an instance of Name');
    this._name = a;
    return this;
};
Metadata.prototype.getName = function(){
    return this._name;
};
Metadata.prototype.unsetName = function(){
    this._name = null;
    return this;
};

Metadata.prototype.setUuid = function(a){
    if (! (a instanceof Uuid))
        throw new Error('arguments to Metadata.setUuid must be an instance of Uuid');
    this._uuid = a;
    return this;
};
Metadata.prototype.getUuid = function(){
    return this._uuid;
};
Metadata.prototype.unsetUuid = function(){
    this._uuid = null;
    return this;
};

module.exports = {
    Metadata: Metadata,
    Authors: Authors,
    Description: Description,
    Name: Name,
    Uuid: Uuid,
};
