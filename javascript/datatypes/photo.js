'use strict';
 

function Uuid(s) {
    var rex = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/;
    if (typeof s !== 'string' || !s.match(rex))
        throw new Error('photo uuid must be a string that looks like a uuid');
    this._uuid = s;
    return this;
};
Uuid.fromData = function(a){ return new Uuid(a); };
Uuid.prototype.toData = function(){ return this._uuid; };


function RealLocation(longitude, latitude, radius) {
    if (typeof longitude !== 'number')
        throw new Error('photo real-location longitude must be a number');
    if (typeof latitude !== 'number')
        throw new Error('photo real-location latitude must be a number');
    if (typeof radius !== 'number')
        throw new Error('photo real-location meterRadius must be a number');
    if (longitude < -180 || 180 < longitude)
        throw new Error('photo real-location longitude must be between -180 and 180');
    if (latitude < -90 || 90 < latitude)
        throw new Error('photo real-location latitude must be between -90 and 90');
    if (radius < 0)
        throw new Error('photo real-location meterRadius must not be negative');
    this._longitude = longitude;
    this._latitude = latitude;
    this._radius = radius;
    return this;
};
RealLocation.fromData = function(j){
    return new RealLocation(j.longitude, j.latitude, j.meterRadius);
};
RealLocation.prototype.toData = function(){
    return {
        longitude: this._longitude,
        latitude: this._latitude,
        meterRadius: this._radius,
    };
};

function RealDatetimeoriginal(dto) {
    if (typeof dto !== 'string')
        throw new Error('photo name is not a string');
    if (dto === '')
        throw new Error('photo name is an empty string');
    this._dto = dto;
    return this;
};
RealDatetimeoriginal.fromData = function(n){ return new RealDatetimeoriginal(n); };
RealDatetimeoriginal.prototype.toData = function(){ return this._dto; };


function RealOrientation(orientation) {
    if (typeof orientation !== 'number')
        throw new Error('photo real-orientation is not a number');
    if (orientation < 1 || 8 < orientation || (orientation % 1) !== 0)
        throw new Error('photo real-orientation must be 1, 2, 3, 4, 5, 6, 7, or 8');
    this._orientation = orientation;
    return this;
};
RealOrientation.fromData = function(n){ return new RealOrientation(n); };
RealOrientation.prototype.toData = function(){ return this._orientation; };

function Fields(fields) {
    if (typeof fields !== 'object')
        throw new Error('photo fields must be an object');
    var good = {};
    for (var field in fields) {
        if (typeof fields[field] !== 'string')
            throw new Error('each photo field value must be string');
        good[field] = fields[field];
    }
    this._fields = good;
    return this;
};
Fields.fromData = function(n){ return new Fields(n); };
Fields.prototype.toData = function(){
   var j = {};
   for (var f in this._fields) j[f] = this._fields[f];
   return j;
};

function ClimbLayer(climbUuid, path) {
    var rex = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/;
    if (typeof climbUuid !== 'string' || !climbUuid.match(rex))
        throw new Error('photo climb-layer.climb-uuid must be a string that looks like a uuid');
    this._climbUuid = climbUuid;
    if (!Array.isArray(path) || path.length < 2)
        throw new Error('photo climb-layer.path must be an array with more than 2 points');
    this._path = [];
    for (var i = 0; i < path.length; i++) {
        // check bounds for topOffset and leftOffset
        if (!path[i].hasOwnProperty('leftOffset'))
            throw new Error('each climb-layer point should have a leftOffset');
        if (!path[i].hasOwnProperty('topOffset'))
            throw new Error('each climb-layer point should have a topOffset');
        if (Object.keys(path[i]).length > 2)
            throw new Error('each point on a climb-layer\'s path should have just two fields: leftOffset and topOffset');
        this._path.push([path[i].leftOffset, path[i].topOffset]);
    }
    return this;
};
ClimbLayer.fromData = function(d){
    if (typeof d !== 'object')
        throw new Error('each climb-layer must be an object');
    if (!d.hasOwnProperty('path'))
        throw new Error('each climb-layer point should have a path');
    if (!d.hasOwnProperty('climbUuid'))
        throw new Error('each climb-layer point should have a climbUuid');
    if (Object.keys(d).length > 2)
        throw new Error('each climb-layer should have just two fields: climbUuid and path');
    return new ClimbLayer(d.climbUuid, d.path);
};
ClimbLayer.prototype.toData = function(){
    return {
        climbUuid: this._climbUuid,
        path: this._path.map(function(pt){
            return {leftOffset: pt[0], topOffset: pt[1]};
        })
    };
};
ClimbLayer.prototype.copy = function(){
    return ClimbLayer.fromData(this.toData()); 
};


function ClimbLayers(layers) {
    if (!Array.isArray(layers))
        throw new Error('climb-layers must be an array');
    this._layers = layers.map(l => l.copy());
    return this;
};
ClimbLayers.fromData = function(d){
    if (!Array.isArray(d))
        throw new Error('climb-layers must be an array');
    let layers = d.map(l => ClimbLayer.fromData(l));
    return new ClimbLayers(layers);
};
ClimbLayers.prototype.toData = function(){
   return this._layers.map(l => l.toData());
};


function AreaLayer(areaUuid, polygon) {
    var rex = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/;
    if (typeof areaUuid !== 'string' || !areaUuid.match(rex))
        throw new Error('photo area-layer.area-uuid must be a string that looks like a uuid');
    this._areaUuid = areaUuid;
    if (!Array.isArray(polygon) || polygon.length < 4)
        throw new Error('photo area-layer.polygon must be an array with more than 2 points');
    this._polygon = [];
    for (var i = 0; i < polygon.length; i++) {
        // check bounds for topOffset and leftOffset
        if (!polygon[i].hasOwnProperty('leftOffset'))
            throw new Error('each area-layer point should have a leftOffset');
        if (!polygon[i].hasOwnProperty('topOffset'))
            throw new Error('each area-layer point should have a topOffset');
        if (Object.keys(polygon[i]).length > 2)
            throw new Error('each point on a area-layer\'s polygon should have just two fields: leftOffset and topOffset');
        this._polygon.push([polygon[i].leftOffset, polygon[i].topOffset]);
    }
    let first = this._polygon[0];
    let last = this._polygon[this._polygon.length - 1];
    if ((first[0] !== last[0]) || (first[1] !== last[1]))
        throw new Error('an area-layer\'s polygon should start and stop on the same point');
    return this;
};
AreaLayer.fromData = function(d){
    if (typeof d !== 'object')
        throw new Error('each area-layer must be an object');
    if (!d.hasOwnProperty('polygon'))
        throw new Error('each area-layer point should have a "polygon" property');
    if (!d.hasOwnProperty('areaUuid'))
        throw new Error('each area-layer point should have an "areaUuid" property');
    if (Object.keys(d).length > 2)
        throw new Error('each area-layer should have just two fields: areaUuid and polygon');
    return new AreaLayer(d.areaUuid, d.polygon);
};
AreaLayer.prototype.toData = function(){
    return {
        areaUuid: this._areaUuid,
        polygon: this._polygon.map(function(pt){
            return {leftOffset: pt[0], topOffset: pt[1]};
        })
    };
};
AreaLayer.prototype.copy = function(){
    return AreaLayer.fromData(this.toData()); 
};


function AreaLayers(layers) {
    if (!Array.isArray(layers))
        throw new Error('area-layers must be an array');
    this._layers = layers.map(l => l.copy());
    return this;
};
AreaLayers.fromData = function(d){
    if (!Array.isArray(d))
        throw new Error('area-layers must be an array');
    let layers = d.map(l => AreaLayer.fromData(l));
    return new AreaLayers(layers);
};
AreaLayers.prototype.toData = function(){
   return this._layers.map(l => l.toData());
};


function Photo(){
    this._areaLayers = null;
    this._climbLayers = null;
    this._fields = null;
    this._realDatetimeoriginal = null;
    this._realLocation = null;
    this._realOrientation = null;
    this._uuid = null;
    return this;
};
Photo.prototype.toData = function(){
    let d = {};
    if (this._areaLayers)
       d.areaLayers = this._areaLayers.toData();
    if (this._climbLayers)
       d.climbLayers = this._climbLayers.toData();
    if (this._fields)
       d.fields = this._fields.toData();
    if (this._realDatetimeoriginal)
       d.realDatetimeoriginal = this._realDatetimeoriginal.toData();
    if (this._realLocation)
       d.realLocation = this._realLocation.toData();
    if (this._realOrientation)
       d.realOrientation = this._realOrientation.toData();
    if (this._uuid)
       d.uuid = this._uuid.toData();
    return d;
};
Photo.fromData = function(d){
    let a = new Photo();
    for (var k in d) {
        if (k === 'areaLayers')
            a.setAreaLayers(AreaLayers.fromData(d[k]));
        else if (k === 'climbLayers')
            a.setClimbLayers(ClimbLayers.fromData(d[k]));
        else if (k === 'fields')
            a.setFields(Fields.fromData(d[k]));
        else if (k === 'realDatetimeoriginal')
            a.setRealDatetimeoriginal(RealDatetimeoriginal.fromData(d[k]));
        else if (k === 'realLocation')
            a.setRealLocation(RealLocation.fromData(d[k]));
        else if (k === 'realOrientation')
            a.setRealOrientation(RealOrientation.fromData(d[k]));
        else if (k === 'uuid')
            a.setUuid(Uuid.fromData(d[k]));
        else
            throw new Error('unexpected key: ' + k);
    }
    return a;
};

Photo.prototype.setAreaLayers = function(a){
    if (! (a instanceof AreaLayers))
        throw new Error('arguments to Photo.setAreaLayers must be an instance of AreaLayers');
    this._areaLayers = a;
    return this;
};
Photo.prototype.getAreaLayers = function(){
    return this._areaLayers;
};
Photo.prototype.unsetAreaLayers = function(){
    this._areaLayers = null;
    return this;
};

Photo.prototype.setClimbLayers = function(a){
    if (! (a instanceof ClimbLayers))
        throw new Error('arguments to Photo.setClimbLayers must be an instance of ClimbLayers');
    this._climbLayers = a;
    return this;
};
Photo.prototype.getClimbLayers = function(){
    return this._climbLayers;
};
Photo.prototype.unsetClimbLayers = function(){
    this._climbLayers = null;
    return this;
};

Photo.prototype.setFields = function(a){
    if (! (a instanceof Fields))
        throw new Error('arguments to Photo.setFields must be an instance of Fields');
    this._fields = a;
    return this;
};
Photo.prototype.getFields = function(){
    return this._fields;
};
Photo.prototype.unsetFields = function(){
    this._fields = null;
    return this;
};

Photo.prototype.setRealDatetimeoriginal = function(a){
    if (! (a instanceof RealDatetimeoriginal))
        throw new Error('arguments to Photo.setRealDatetimeoriginal must be an instance of RealDatetimeoriginal');
    this._realDatetimeoriginal = a;
    return this;
};
Photo.prototype.getRealDatetimeoriginal = function(){
    return this._realDatetimeoriginal;
};
Photo.prototype.unsetRealDatetimeoriginal = function(){
    this._realDatetimeoriginal = null;
    return this;
};

Photo.prototype.setRealLocation = function(a){
    if (! (a instanceof RealLocation))
        throw new Error('arguments to Photo.setRealLocation must be an instance of RealLocation');
    this._realLocation = a;
    return this;
};
Photo.prototype.getRealLocation = function(){
    return this._realLocation;
};
Photo.prototype.unsetRealLocation = function(){
    this._realLocation = null;
    return this;
};

Photo.prototype.setRealOrientation = function(a){
    if (! (a instanceof RealOrientation))
        throw new Error('arguments to Photo.setRealOrientation must be an instance of RealOrientation');
    this._realOrientation = a;
    return this;
};
Photo.prototype.getRealOrientation = function(){
    return this._realOrientation;
};
Photo.prototype.unsetRealOrientation = function(){
    this._realOrientation = null;
    return this;
};

Photo.prototype.setUuid = function(a){
    if (! (a instanceof Uuid))
        throw new Error('arguments to Photo.setUuid must be an instance of Uuid');
    this._uuid = a;
    return this;
};
Photo.prototype.getUuid = function(){
    return this._uuid;
};
Photo.prototype.unsetUuid = function(){
    this._uuid = null;
    return this;
};

module.exports = {
    Photo: Photo,
    AreaLayers: AreaLayers,
    ClimbLayers: ClimbLayers,
    Fields: Fields,
    RealDatetimeoriginal: RealDatetimeoriginal,
    RealLocation: RealLocation,
    RealOrientation: RealOrientation,
    Uuid: Uuid,
};
