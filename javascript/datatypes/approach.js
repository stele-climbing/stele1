'use strict';

function Name(n) {
    if (typeof n !== 'string' || n === '')
        throw new Error('approach name must be a non-empty string');
    this._name = n;
    return this;
};
Name.fromData = function(n){ return new Name(n); };
Name.prototype.toData = function(){ return this._name; };


function Uuid(s) {
    var rex = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/;
    if (typeof s !== 'string' || !s.match(rex))
        throw new Error('approach uuid must be a string that looks like a uuid');
    this._uuid = s;
    return this;
};
Uuid.fromData = function(a){ return new Uuid(a); };
Uuid.prototype.toData = function(){ return this._uuid; };


function Description(desc) {
    if (typeof desc !== 'string' || desc === '')
        throw new Error('approach description must be a non-empty string');
    this._desc = desc;
    return this;
};
Description.fromData = function(j){ return new Description(j); };
Description.prototype.toData = function(){ return this._desc; };

function Path(points) {
    if (!Array.isArray(points) || points.length < 2)
        throw new Error('approach path must be an array with two or more points');
    var good = [];
    for (var i = 0; i < points.length; i++) {
        if (typeof points[i].longitude !== 'number')
            throw new Error('approach path points\' longitude must be a number' + JSON.stringify(points[i]));
        if (typeof points[i].latitude !== 'number')
            throw new Error('approach path points\' latitude must be a number' +  JSON.stringify(points[i]));
        if (points[i].longitude < -180 || 180 < points[i].longitude)
            throw new Error('approach path points\' longitude must be between -180 and 180');
        if (points[i].latitude < -90 || 90 < points[i].latitude)
            throw new Error('approach path points\' latitude must be between -90 and 90');
        good.push([points[i].longitude, points[i].latitude]);
    }
    this._points = good;
    return this;
};
Path.fromData = function(j){
    return new Path(j);
};
Path.prototype.toData = function(){
    return this._points.map(function(pt){
        return {
            longitude: pt[0],
            latitude: pt[1],
        };
    });
};

function Approach(){
    this._description = null;
    this._name = null;
    this._path = null;
    this._uuid = null;
    return this;
};
Approach.prototype.toData = function(){
    let d = {};
    if (this._description)
       d.description = this._description.toData();
    if (this._name)
       d.name = this._name.toData();
    if (this._path)
       d.path = this._path.toData();
    if (this._uuid)
       d.uuid = this._uuid.toData();
    return d;
};
Approach.fromData = function(d){
    let a = new Approach();
    for (var k in d) {
        if (k === 'description')
            a.setDescription(Description.fromData(d[k]));
        else if (k === 'name')
            a.setName(Name.fromData(d[k]));
        else if (k === 'path')
            a.setPath(Path.fromData(d[k]));
        else if (k === 'uuid')
            a.setUuid(Uuid.fromData(d[k]));
        else
            throw new Error('unexpected key: ' + k);
    }
    return a;
};

Approach.prototype.setDescription = function(a){
    if (! (a instanceof Description))
        throw new Error('arguments to Approach.setDescription must be an instance of Description');
    this._description = a;
    return this;
};
Approach.prototype.getDescription = function(){
    return this._description;
};
Approach.prototype.unsetDescription = function(){
    this._description = null;
    return this;
};

Approach.prototype.setName = function(a){
    if (! (a instanceof Name))
        throw new Error('arguments to Approach.setName must be an instance of Name');
    this._name = a;
    return this;
};
Approach.prototype.getName = function(){
    return this._name;
};
Approach.prototype.unsetName = function(){
    this._name = null;
    return this;
};

Approach.prototype.setPath = function(a){
    if (! (a instanceof Path))
        throw new Error('arguments to Approach.setPath must be an instance of Path');
    this._path = a;
    return this;
};
Approach.prototype.getPath = function(){
    return this._path;
};
Approach.prototype.unsetPath = function(){
    this._path = null;
    return this;
};

Approach.prototype.setUuid = function(a){
    if (! (a instanceof Uuid))
        throw new Error('arguments to Approach.setUuid must be an instance of Uuid');
    this._uuid = a;
    return this;
};
Approach.prototype.getUuid = function(){
    return this._uuid;
};
Approach.prototype.unsetUuid = function(){
    this._uuid = null;
    return this;
};

module.exports = {
    Approach: Approach,
    Description: Description,
    Name: Name,
    Path: Path,
    Uuid: Uuid,
};
