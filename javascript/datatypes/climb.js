'use strict';

function Name(n) {
    if (typeof n !== 'string')
        throw new Error('climb name is not a string');
    if (n === '')
        throw new Error('climb name is an empty string');
    this._name = n;
    return this;
};
Name.fromData = function(n){ return new Name(n); };
Name.prototype.toData = function(){ return this._name; };


function Uuid(s) {
    var rex = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/;
    if (typeof s !== 'string' || !s.match(rex))
        throw new Error('climb uuid must be a string that looks like a uuid');
    this._uuid = s;
    return this;
};
Uuid.fromData = function(a){ return new Uuid(a); };
Uuid.prototype.toData = function(){ return this._uuid; };


function AlternateNames(ns) {
    if (!Array.isArray(ns))
        throw new Error('climb alternate-names must be an array, got ' + ns.toString());
    this._names = ns.map(function(n){ return new Name(n) });
    return this;
};
AlternateNames.fromData = function(n){ return new AlternateNames(n); };
AlternateNames.prototype.toData = function(){
   return this._names.map(function(n){ return n.toData(); });
};


function Rating(a) {
    let keys = Object.keys(a);
    if (keys.length === 0)
        throw new Error('rating data must contain at least 1 of difficulty, protection, or style');
    for (let i in keys) {
        switch (keys[i]) {
            case 'protection':
                if (typeof a.protection === 'string' && a.protection !== '')
                    this._protection = a.protection;
                else
                    throw new Error('when defined, climb.rating.protection must be a non-empty string');
                break;
            case 'difficulty':
                if (typeof a.difficulty === 'string' && a.difficulty !== '')
                    this._difficulty = a.difficulty;
                else
                    throw new Error('when defined, climb.rating.difficulty must be a non-empty string');
                break;
            case 'style':
                if (typeof a.style === 'string' && a.style !== '')
                    this._style = a.style;
                else
                    throw new Error('when defined, climb.rating.style must be a non-empty string');
                break;
            default:
                throw new Error('unexpected climb.rating property "' + keys[i] + '". ' +
                                'expected difficulty, protection, or style');
        }
    }
    return this;
}
Rating.fromData = function(j){ return new Rating(j); };
Rating.prototype.toData = function(){
    let j = {};
    if (this._difficulty) j.difficulty = this._difficulty;
    if (this._protection) j.protection = this._protection;
    if (this._style)      j.style      = this._style;
    return j;
}


function Length(lower, upper, unit) {
    if (typeof lower !== 'number')
        throw new Error('climb length lower estimate must be a number');
    if (typeof upper !== 'number')
        throw new Error('climb length upper estimate must be a number');
    if (typeof unit !== 'string')
        throw new Error('climb length unit must be a string');
    if (lower <= 0)
        throw new Error('climb length lower estimate must be greater than 0');
    if (upper <= 0)
        throw new Error('climb length upper estimate must be greater than 0 ');
    if (upper < lower)
        throw new Error('climb length lower estimate must be lower (or equal to) the upper estimate');
    if (unit === '')
        throw new Error('climb length unit must be a non-empty string');
    this._lower = lower;
    this._upper = upper;
    this._unit = unit;
    return this;
};
Length.fromData = function(j){ return new Length(j.lowerEstimate, j.upperEstimate, j.unit); };
Length.prototype.toData = function(){
    return {
        lowerEstimate: this._lower,
        upperEstimate: this._upper,
        unit: this._unit,
    };
};

function Location(longitude, latitude, radius) {
    if (typeof longitude !== 'number')
        throw new Error('climb location longitude must be a number');
    if (typeof latitude !== 'number')
        throw new Error('climb location latitude must be a number');
    if (typeof radius !== 'number')
        throw new Error('climb location meterRadius must be a number');
    if (longitude < -180 || 180 < longitude)
        throw new Error('climb location longitude must be between -180 and 180');
    if (latitude < -90 || 90 < latitude)
        throw new Error('climb location latitude must be between -90 and 90');
    if (radius < 0)
        throw new Error('climb location meterRadius must not be negative');
    this._longitude = longitude;
    this._latitude = latitude;
    this._radius = radius;
    return this;
};
Location.fromData = function(j){
    return new Location(j.longitude, j.latitude, j.meterRadius);
};
Location.prototype.toData = function(){
    return {
        longitude: this._longitude,
        latitude: this._latitude,
        meterRadius: this._radius,
    };
};


function Ascents(ascents) {
    var a = [];
    if (!Array.isArray(ascents))
        throw new Error('climb ascents must be an array, got ' + ns.toString());
    for (var i = 0; i < ascents.length; i++) {
        if (typeof ascents[i] !== 'string')
            throw new Error('climb ascent must be a string');
        if (ascents[i] === '')
            throw new Error('climb ascent must be a non-empty string');
        a.push(ascents[i]);
    }
    this._ascents = a;
    return this;
};
Ascents.fromData = function(j){ return new Ascents(j); };
Ascents.prototype.toData = function(){
    return this._ascents.map(function(n){ return n });
};

function ProjectStatus(stat) {
    if (typeof stat !== 'string' && stat !== null)
        throw new Error('climb project-status must not be a string or null');
    if (typeof stat === 'string' && stat === '')
        throw new Error('climb project-status must not be a non-empty string (or null)');
    this._status = stat;
    return this;
};
ProjectStatus.fromData = function(n){ return new ProjectStatus(n); };
ProjectStatus.prototype.toData = function(){ return this._status; };

function Steepness(segs) {
    if (!Array.isArray(segs))
        throw new Error('climb steepness must be an array');
    if (segs.length === 0)
        throw new Error('climb steepness must be a non-empty array');
    var a = [];
    for (var i = 0; i < segs.length; i++) {
        if (typeof segs[i].startingAt !== 'number' || segs[i].startingAt < 0 || 1 < segs[i].startingAt)
            throw new Error('climb steepness segment\'s startingAt value must be a number from 0 to 1');
        if (typeof segs[i].degreesOverhung !== 'number' || segs[i].degreesOverhung < -180 || 180 < segs[i].degreesOverhung)
            throw new Error('climb steepness segment\'s degreesOverhung value must be a number from -180 to 180');
        if (i === 0) {
            if (segs[i].startingAt !== 0)
                throw new Error('climb steepness\' first segment must have a startingAt value of 0');
        } else {
            if (segs[i].startingAt <= a[a.length - 1][0])
                throw new Error('climb steepness segments startingAt values must increase sequentially');
        }
        a.push([segs[i].startingAt, segs[i].degreesOverhung]);
    } 
    this._segments = a;
    return this;
}
Steepness.fromData = function(n){ return new Steepness(n); };
Steepness.prototype.toData = function(){
    return this._segments.map(function(s){
        return {startingAt: s[0], degreesOverhung: s[1]};
    });
};

function Terrain(segs) {
    if (!Array.isArray(segs) || segs.length === 0)
        throw new Error('climb terrain must be a non-empty array');
    var a = [];
    for (var i = 0; i < segs.length; i++) {
        if (typeof segs[i].startingAt !== 'number' || segs[i].startingAt < 0 || 1 < segs[i].startingAt)
            throw new Error('climb terrain segment\'s startingAt value must be a number from 0 to 1, got ' + JSON.stringify(segs[i]));
        if (typeof segs[i].description !== 'string' || segs[i].description === '')
            throw new Error('climb terrain segment\'s description must be a non-empty string');
        if (i === 0) {
            if (segs[i].startingAt !== 0)
                throw new Error('climb terrain\'s first segment must have a startingAt value of 0');
        } else {
            if (segs[i].startingAt <= a[a.length - 1][0])
                throw new Error('climb terrain segments startingAt values must increase sequentially');
        }
        a.push([segs[i].startingAt, segs[i].description]);
    } 
    this._segments = a;
    return this;
}
Terrain.fromData = function(n){ return new Terrain(n); };
Terrain.prototype.toData = function(){
    return this._segments.map(function(s){
        return {startingAt: s[0], description: s[1]};
    });
};

function Tags(tags) {
    if (!Array.isArray(tags))
        throw new Error('climb alternate-names must be an array, got ' + tags.toString());
    var good = [];
    for (var i = 0; i < tags.length; i++) {
        if (typeof tags[i] !== 'string' || tags[i] === '')
            throw new Error('each climb tag must be an a non-empty string');
        good.push(tags[i])
    }
    this._tags = good;
    return this;
};
Tags.fromData = function(n){ return new Tags(n); };
Tags.prototype.toData = function(){
   return this._tags.map(function(n){ return n; });
};

function Resources(rs) {
    if (!Array.isArray(rs))
        throw new Error('climb resources must be an array, got ' + rs.toString());
    var good = [];
    for (let r of rs) {
        let a = {};
        if (typeof r?.resource !== 'string' || r?.resource === '')
            throw new Error('each item in climb resources must have the field .resource as a non-empty string');
        a.resource = r.resource;
        if (r.title) {
            if (typeof r.title !== 'string' || r.title === '') {
                throw new Error('when defined, the .title field of a climb resource must be a non-empty string');
            }
            a.title = r.title;
        }
        if (r.description) {
            if (typeof r.description !== 'string' || r.description === '') {
                throw new Error('when defined, the .description field of a climb resource must be a non-empty string');
            }
            a.description = r.description;
        }
        good.push(a)
    }
    this._resources = good;
    return this;
};
Resources.fromData = function(n){ return new Resources(n); };
Resources.prototype.toData = function(){
   return this._resources.map(function(n){ return Object.assign({}, n); });
};

function Fields(fields) {
    if (typeof fields !== 'object')
        throw new Error('climb fields must be an object');
    var good = {};
    for (var field in fields) {
        if (typeof fields[field] !== 'string')
            throw new Error('each climb field value must be string');
        good[field] = fields[field];
    }
    this._fields = good;
    return this;
};
Fields.fromData = function(n){ return new Fields(n); };
Fields.prototype.toData = function(){
   var j = {};
   for (var f in this._fields) j[f] = this._fields[f];
   return j;
};

function Notes(notes) {
    if (!Array.isArray(notes))
        throw new Error('climb notes must be an array');
    var good = [];
    var topics = [];
    for (var i = 0; i < notes.length; i++) {
        if (typeof notes[i].topic !== 'string' || notes[i].topic === '')
            throw new Error('all climb note topics must be a non-empty string');
        if (topics.indexOf(notes[i].topic) !== -1)
            throw new Error('all climb note topics be unique for a climb');
        if (typeof notes[i].content !== 'string')
            throw new Error('all climb note content must be a string');
        good.push({
            topic: notes[i].topic,
            content: notes[i].content
        });
    } 
    this._notes = good;
    return this;
}
Notes.fromData = function(n){ return new Notes(n); };
Notes.prototype.toData = function(){
    return this._notes.map(function(n){
        return {topic: n.topic, content: n.content};
    });
};


function Climb(){
    this._alternateNames = null;
    this._ascents = null;
    this._fields = null;
    this._length = null;
    this._location = null;
    this._name = null;
    this._notes = null;
    this._projectStatus = null;
    this._rating = null;
    this._resources = null;
    this._steepness = null;
    this._tags = null;
    this._terrain = null;
    this._uuid = null;
    return this;
};
Climb.prototype.toData = function(){
    let d = {};
    if (this._alternateNames)
       d.alternateNames = this._alternateNames.toData();
    if (this._ascents)
       d.ascents = this._ascents.toData();
    if (this._fields)
       d.fields = this._fields.toData();
    if (this._length)
       d.length = this._length.toData();
    if (this._location)
       d.location = this._location.toData();
    if (this._name)
       d.name = this._name.toData();
    if (this._notes)
       d.notes = this._notes.toData();
    if (this._projectStatus)
       d.projectStatus = this._projectStatus.toData();
    if (this._rating)
       d.rating = this._rating.toData();
    if (this._resources)
       d.resources = this._resources.toData();
    if (this._steepness)
       d.steepness = this._steepness.toData();
    if (this._tags)
       d.tags = this._tags.toData();
    if (this._terrain)
       d.terrain = this._terrain.toData();
    if (this._uuid)
       d.uuid = this._uuid.toData();
    return d;
};
Climb.fromData = function(d){
    let a = new Climb();
    for (var k in d) {
        if (k === 'alternateNames')
            a.setAlternateNames(AlternateNames.fromData(d[k]));
        else if (k === 'ascents')
            a.setAscents(Ascents.fromData(d[k]));
        else if (k === 'fields')
            a.setFields(Fields.fromData(d[k]));
        else if (k === 'length')
            a.setLength(Length.fromData(d[k]));
        else if (k === 'location')
            a.setLocation(Location.fromData(d[k]));
        else if (k === 'name')
            a.setName(Name.fromData(d[k]));
        else if (k === 'notes')
            a.setNotes(Notes.fromData(d[k]));
        else if (k === 'projectStatus')
            a.setProjectStatus(ProjectStatus.fromData(d[k]));
        else if (k === 'rating')
            a.setRating(Rating.fromData(d[k]));
        else if (k === 'resources')
            a.setResources(Resources.fromData(d[k]));
        else if (k === 'steepness')
            a.setSteepness(Steepness.fromData(d[k]));
        else if (k === 'tags')
            a.setTags(Tags.fromData(d[k]));
        else if (k === 'terrain')
            a.setTerrain(Terrain.fromData(d[k]));
        else if (k === 'uuid')
            a.setUuid(Uuid.fromData(d[k]));
        else
            throw new Error('unexpected key: ' + k);
    }
    return a;
};

Climb.prototype.setAlternateNames = function(a){
    if (! (a instanceof AlternateNames))
        throw new Error('arguments to Climb.setAlternateNames must be an instance of AlternateNames');
    this._alternateNames = a;
    return this;
};
Climb.prototype.getAlternateNames = function(){
    return this._alternateNames;
};
Climb.prototype.unsetAlternateNames = function(){
    this._alternateNames = null;
    return this;
};

Climb.prototype.setAscents = function(a){
    if (! (a instanceof Ascents))
        throw new Error('arguments to Climb.setAscents must be an instance of Ascents');
    this._ascents = a;
    return this;
};
Climb.prototype.getAscents = function(){
    return this._ascents;
};
Climb.prototype.unsetAscents = function(){
    this._ascents = null;
    return this;
};

Climb.prototype.setFields = function(a){
    if (! (a instanceof Fields))
        throw new Error('arguments to Climb.setFields must be an instance of Fields');
    this._fields = a;
    return this;
};
Climb.prototype.getFields = function(){
    return this._fields;
};
Climb.prototype.unsetFields = function(){
    this._fields = null;
    return this;
};

Climb.prototype.setLength = function(a){
    if (! (a instanceof Length))
        throw new Error('arguments to Climb.setLength must be an instance of Length');
    this._length = a;
    return this;
};
Climb.prototype.getLength = function(){
    return this._length;
};
Climb.prototype.unsetLength = function(){
    this._length = null;
    return this;
};

Climb.prototype.setLocation = function(a){
    if (! (a instanceof Location))
        throw new Error('arguments to Climb.setLocation must be an instance of Location');
    this._location = a;
    return this;
};
Climb.prototype.getLocation = function(){
    return this._location;
};
Climb.prototype.unsetLocation = function(){
    this._location = null;
    return this;
};

Climb.prototype.setName = function(a){
    if (! (a instanceof Name))
        throw new Error('arguments to Climb.setName must be an instance of Name');
    this._name = a;
    return this;
};
Climb.prototype.getName = function(){
    return this._name;
};
Climb.prototype.unsetName = function(){
    this._name = null;
    return this;
};

Climb.prototype.setNotes = function(a){
    if (! (a instanceof Notes))
        throw new Error('arguments to Climb.setNotes must be an instance of Notes');
    this._notes = a;
    return this;
};
Climb.prototype.getNotes = function(){
    return this._notes;
};
Climb.prototype.unsetNotes = function(){
    this._notes = null;
    return this;
};

Climb.prototype.setProjectStatus = function(a){
    if (! (a instanceof ProjectStatus))
        throw new Error('arguments to Climb.setProjectStatus must be an instance of ProjectStatus');
    this._projectStatus = a;
    return this;
};
Climb.prototype.getProjectStatus = function(){
    return this._projectStatus;
};
Climb.prototype.unsetProjectStatus = function(){
    this._projectStatus = null;
    return this;
};

Climb.prototype.setRating = function(a){
    if (! (a instanceof Rating))
        throw new Error('arguments to Climb.setRating must be an instance of Rating');
    this._rating = a;
    return this;
};
Climb.prototype.getRating = function(){
    return this._rating;
};
Climb.prototype.unsetRating = function(){
    this._rating = null;
    return this;
};

Climb.prototype.setResources = function(a){
    if (! (a instanceof Resources))
        throw new Error('arguments to Climb.setResources must be an instance of Resources');
    this._resources = a;
    return this;
};
Climb.prototype.getResources = function(){
    return this._resources;
};
Climb.prototype.unsetResources = function(){
    this._resources = null;
    return this;
};

Climb.prototype.setSteepness = function(a){
    if (! (a instanceof Steepness))
        throw new Error('arguments to Climb.setSteepness must be an instance of Steepness');
    this._steepness = a;
    return this;
};
Climb.prototype.getSteepness = function(){
    return this._steepness;
};
Climb.prototype.unsetSteepness = function(){
    this._steepness = null;
    return this;
};

Climb.prototype.setTags = function(a){
    if (! (a instanceof Tags))
        throw new Error('arguments to Climb.setTags must be an instance of Tags');
    this._tags = a;
    return this;
};
Climb.prototype.getTags = function(){
    return this._tags;
};
Climb.prototype.unsetTags = function(){
    this._tags = null;
    return this;
};

Climb.prototype.setTerrain = function(a){
    if (! (a instanceof Terrain))
        throw new Error('arguments to Climb.setTerrain must be an instance of Terrain');
    this._terrain = a;
    return this;
};
Climb.prototype.getTerrain = function(){
    return this._terrain;
};
Climb.prototype.unsetTerrain = function(){
    this._terrain = null;
    return this;
};

Climb.prototype.setUuid = function(a){
    if (! (a instanceof Uuid))
        throw new Error('arguments to Climb.setUuid must be an instance of Uuid');
    this._uuid = a;
    return this;
};
Climb.prototype.getUuid = function(){
    return this._uuid;
};
Climb.prototype.unsetUuid = function(){
    this._uuid = null;
    return this;
};

module.exports = {
    Climb: Climb,
    AlternateNames: AlternateNames,
    Ascents: Ascents,
    Fields: Fields,
    Length: Length,
    Location: Location,
    Name: Name,
    Notes: Notes,
    ProjectStatus: ProjectStatus,
    Rating: Rating,
    Resources: Resources,
    Steepness: Steepness,
    Tags: Tags,
    Terrain: Terrain,
    Uuid: Uuid,
};
