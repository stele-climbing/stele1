'use strict';

function Name(n) {
    if (typeof n !== 'string' || n === '')
        throw new Error('parking description must be a non-empty string');
    this._name = n;
    return this;
};
Name.fromData = function(n){ return new Name(n); };
Name.prototype.toData = function(){ return this._name; };


function Uuid(s) {
    var rex = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/;
    if (typeof s !== 'string' || !s.match(rex))
        throw new Error('parking uuid must be a string that looks like a uuid');
    this._uuid = s;
    return this;
};
Uuid.fromData = function(a){ return new Uuid(a); };
Uuid.prototype.toData = function(){ return this._uuid; };


function Description(n) {
    if (typeof n !== 'string' || n === '')
        throw new Error('parking description must be a non-empty string');
    this._desc = n;
    return this;
};
Description.fromData = function(j){ return new Description(j); };
Description.prototype.toData = function(){ return this._desc; };

function Capacity(n) {
    if (typeof n !== 'number' || n <= 0)
        throw new Error('parking capacity must be a number greater than 0');
    this._capacity = n;
    return this;
};
Capacity.fromData = function(j){ return new Capacity(j); };
Capacity.prototype.toData = function(){ return this._capacity; };

function Location(longitude, latitude) {
    if (typeof longitude !== 'number')
        throw new Error('parking location longitude must be a number');
    if (typeof latitude !== 'number')
        throw new Error('parking location latitude must be a number');
    if (longitude < -180 || 180 < longitude)
        throw new Error('parking location longitude must be between -180 and 180');
    if (latitude < -90 || 90 < latitude)
        throw new Error('parking location latitude must be between -90 and 90');
    this._longitude = longitude;
    this._latitude = latitude;
    return this;
};
Location.fromData = function(j){
    return new Location(j.longitude, j.latitude);
};
Location.prototype.toData = function(){
    return {
        longitude: this._longitude,
        latitude: this._latitude,
    };
};


function Parking(){
    this._capacity = null;
    this._description = null;
    this._location = null;
    this._name = null;
    this._uuid = null;
    return this;
};
Parking.prototype.toData = function(){
    let d = {};
    if (this._capacity)
       d.capacity = this._capacity.toData();
    if (this._description)
       d.description = this._description.toData();
    if (this._location)
       d.location = this._location.toData();
    if (this._name)
       d.name = this._name.toData();
    if (this._uuid)
       d.uuid = this._uuid.toData();
    return d;
};
Parking.fromData = function(d){
    let a = new Parking();
    for (var k in d) {
        if (k === 'capacity')
            a.setCapacity(Capacity.fromData(d[k]));
        else if (k === 'description')
            a.setDescription(Description.fromData(d[k]));
        else if (k === 'location')
            a.setLocation(Location.fromData(d[k]));
        else if (k === 'name')
            a.setName(Name.fromData(d[k]));
        else if (k === 'uuid')
            a.setUuid(Uuid.fromData(d[k]));
        else
            throw new Error('unexpected key: ' + k);
    }
    return a;
};

Parking.prototype.setCapacity = function(a){
    if (! (a instanceof Capacity))
        throw new Error('arguments to Parking.setCapacity must be an instance of Capacity');
    this._capacity = a;
    return this;
};
Parking.prototype.getCapacity = function(){
    return this._capacity;
};
Parking.prototype.unsetCapacity = function(){
    this._capacity = null;
    return this;
};

Parking.prototype.setDescription = function(a){
    if (! (a instanceof Description))
        throw new Error('arguments to Parking.setDescription must be an instance of Description');
    this._description = a;
    return this;
};
Parking.prototype.getDescription = function(){
    return this._description;
};
Parking.prototype.unsetDescription = function(){
    this._description = null;
    return this;
};

Parking.prototype.setLocation = function(a){
    if (! (a instanceof Location))
        throw new Error('arguments to Parking.setLocation must be an instance of Location');
    this._location = a;
    return this;
};
Parking.prototype.getLocation = function(){
    return this._location;
};
Parking.prototype.unsetLocation = function(){
    this._location = null;
    return this;
};

Parking.prototype.setName = function(a){
    if (! (a instanceof Name))
        throw new Error('arguments to Parking.setName must be an instance of Name');
    this._name = a;
    return this;
};
Parking.prototype.getName = function(){
    return this._name;
};
Parking.prototype.unsetName = function(){
    this._name = null;
    return this;
};

Parking.prototype.setUuid = function(a){
    if (! (a instanceof Uuid))
        throw new Error('arguments to Parking.setUuid must be an instance of Uuid');
    this._uuid = a;
    return this;
};
Parking.prototype.getUuid = function(){
    return this._uuid;
};
Parking.prototype.unsetUuid = function(){
    this._uuid = null;
    return this;
};

module.exports = {
    Parking: Parking,
    Capacity: Capacity,
    Description: Description,
    Location: Location,
    Name: Name,
    Uuid: Uuid,
};
