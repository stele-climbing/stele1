const path = require('path');
const os = require('os');
const fs = require('fs');

const climb = require('@stele1/datatypes/climb.js');
const approach = require('@stele1/datatypes/approach.js');
const photo = require('@stele1/datatypes/photo.js');
const parking = require('@stele1/datatypes/parking.js');
const area = require('@stele1/datatypes/area.js');
const metadata = require('@stele1/datatypes/metadata.js');


function Project(projectPath){
    this._path_original = projectPath;
    this._path = path.normalize(projectPath);
    return this;
};
Project.prototype._subpath = function(){
    let args = Array.prototype.slice.apply(arguments);
    return path.join.apply(this._path, [this._path].concat(args));
};
Project.prototype.exists = function(){
    // coarse, but good enough
    let metafile = this._subpath('project.json');
    return fs.existsSync(this._path) && fs.existsSync(metafile);
};
Project.prototype._ensure_dir = function(path){
    if (!fs.existsSync(path)) fs.mkdirSync(path)
    return null;
};
Project.prototype._ensure_parent = function(a){
    return this._ensure_dir(path.dirname(a));
};

Project.prototype.create = function(){
    const p = this._path;
    if (fs.existsSync(p)) {
        if (fs.statSync(p).isDirectory()) {
            if (fs.readdirSync(p).length) {
                throw new Error("project can't be created in a non-empty directory. " +
                                'found other contents in ' + p);
            }
        } else {
            throw new Error('project can only be created in a directory.' +
                            'non-directory found at ' + p);
        }
    } else {
        fs.mkdirSync(p, {recursive: true});
    }
    fs.mkdirSync(this._subpath('climbs'));
    fs.mkdirSync(this._subpath('approaches'));
    fs.mkdirSync(this._subpath('photos'));
    fs.mkdirSync(this._subpath('parkings'));
    fs.mkdirSync(this._subpath('areas'));
    fs.mkdirSync(this._subpath('metadata'));
    let versionfile = this._subpath('stele1.txt');
    fs.writeFileSync(versionfile, 'stele1' + os.EOL);
    this.setMetadata(new metadata.Metadata());
};

Project.prototype.getMetadata = function(){
    let f = this._subpath('project.json');
    if (fs.existsSync(f)) {
        let a = JSON.parse(fs.readFileSync(f, 'utf8').toString());
        let b = metadata.Metadata.fromData(a);
        return b;
    } else {
        return null;
    }
};
Project.prototype.setMetadata = function(m){
    let f = this._subpath('project.json');
    this._ensure_parent(f);
    fs.writeFileSync(f, JSON.stringify(m.toData(), null, 4) + os.EOL);
    return null;
};

Project.prototype.climbUuids = function(){
    let dir = this._subpath('climbs');
    this._ensure_dir(dir);
    if (!fs.existsSync(dir)) {
        return [];
    } else {
        return fs.readdirSync(dir)
            .filter(f => f.match(/\.json$/))
            .map(f => new climb.Uuid(f.replace(/\.json$/, '')));
    }
};
Project.prototype.getClimbByUuid = function(uuid){
    let f = this._subpath('climbs', uuid.toData() + '.json');
    this._ensure_parent(f);
    if (fs.existsSync(f)) {
        let a = JSON.parse(fs.readFileSync(f, 'utf8').toString());
        let b = climb.Climb.fromData(a);
        if (uuid.toData() !== b.getUuid().toData())
            throw new Error('filename ' + f +
                ' doesn\'t match uuid of climb ' + b.getUuid().toData());
        return b;
    } else {
        return null;
    }
};
Project.prototype.setClimb = function(climb){
    let uuid = climb.getUuid();
    if (!uuid)
        throw new Error('climb must have a uuid when using setClimb');
    let f = this._subpath('climbs', uuid.toData() + '.json');
    this._ensure_parent(f);
    fs.writeFileSync(f, JSON.stringify(climb.toData(), null, 4) + os.EOL);
    return null;
};
Project.prototype.removeClimbByUuid = function(uuid){
    let f = this._subpath('climbs', uuid.toData() + '.json');
    if (fs.existsSync(f))
        fs.rmSync(f);
    return null;
};

Project.prototype.approachUuids = function(){
    let dir = this._subpath('approaches');
    this._ensure_dir(dir);
    if (!fs.existsSync(dir)) {
        return [];
    } else {
        return fs.readdirSync(dir)
            .filter(f => f.match(/\.json$/))
            .map(f => new approach.Uuid(f.replace(/\.json$/, '')));
    }
};
Project.prototype.getApproachByUuid = function(uuid){
    let f = this._subpath('approaches', uuid.toData() + '.json');
    this._ensure_parent(f);
    if (fs.existsSync(f)) {
        let a = JSON.parse(fs.readFileSync(f, 'utf8').toString());
        let b = approach.Approach.fromData(a);
        if (uuid.toData() !== b.getUuid().toData())
            throw new Error('filename ' + f +
                ' doesn\'t match uuid of approach ' + b.getUuid().toData());
        return b;
    } else {
        return null;
    }
};
Project.prototype.setApproach = function(approach){
    let uuid = approach.getUuid();
    if (!uuid)
        throw new Error('approach must have a uuid when using setApproach');
    let f = this._subpath('approaches', uuid.toData() + '.json');
    this._ensure_parent(f);
    fs.writeFileSync(f, JSON.stringify(approach.toData(), null, 4) + os.EOL);
    return null;
};
Project.prototype.removeApproachByUuid = function(uuid){
    let f = this._subpath('approaches', uuid.toData() + '.json');
    if (fs.existsSync(f))
        fs.rmSync(f);
    return null;
};

Project.prototype.photoUuids = function(){
    let dir = this._subpath('photos');
    this._ensure_dir(dir);
    if (!fs.existsSync(dir)) {
        return [];
    } else {
        return fs.readdirSync(dir)
            .filter(f => f.match(/\.json$/))
            .map(f => new photo.Uuid(f.replace(/\.json$/, '')));
    }
};
Project.prototype.getPhotoByUuid = function(uuid){
    let f = this._subpath('photos', uuid.toData() + '.json');
    this._ensure_parent(f);
    if (fs.existsSync(f)) {
        let a = JSON.parse(fs.readFileSync(f, 'utf8').toString());
        let b = photo.Photo.fromData(a);
        if (uuid.toData() !== b.getUuid().toData())
            throw new Error('filename ' + f +
                ' doesn\'t match uuid of photo ' + b.getUuid().toData());
        return b;
    } else {
        return null;
    }
};
Project.prototype.setPhoto = function(photo){
    let uuid = photo.getUuid();
    if (!uuid)
        throw new Error('photo must have a uuid when using setPhoto');
    let f = this._subpath('photos', uuid.toData() + '.json');
    this._ensure_parent(f);
    fs.writeFileSync(f, JSON.stringify(photo.toData(), null, 4) + os.EOL);
    return null;
};
Project.prototype.removePhotoByUuid = function(uuid){
    let f = this._subpath('photos', uuid.toData() + '.json');
    let p = this.getPhotoImageByUuid(uuid);
    if (fs.existsSync(f))
        fs.rmSync(f);
    if (p && fs.existsSync(p))
        fs.rmSync(p);
    return null;
};
Project.prototype._getPhotoImagePath = function(uuid){
    let pdir = this._subpath('photos');
    let pattern = new RegExp(uuid.toData() + "_image(\.*)?");
    return fs.readdirSync(pdir).find(n => n.match(pattern));
};
Project.prototype.getPhotoImageByUuid = function(uuid, photoPath){
    let img = this._getPhotoImagePath(uuid);
    if (!img) return null;
    return this._subpath('photos', img);
};
Project.prototype.setPhotoImageByUuid = function(uuid, src, opts){
    // TODO: check if the uuid is a real photo
    let setFromFile = (uuid, photoPath) => {
        let existing = this._getPhotoImagePath(uuid);
        let extn = path.basename(photoPath).replace(/[^.]*/, '');
        let target = this._subpath('photos', uuid.toData() + '_image.' + extn);
        this._ensure_parent(target);
        if (existing) fs.rmSync(existing);
        fs.copyFileSync(photoPath, target);
        return null;
    };
    let setFromBuffer = (uuid, buf, opts) => {
        let existing = this._getPhotoImagePath(uuid);
        if (existing) fs.unlink(existing);
        let target = this._subpath('photos', uuid.toData() + '_image.' + opts.extension);
        this._ensure_parent(target);
        fs.writeFileSync(target, buf);
        return null;
    }
    if      (typeof src === 'string') return setFromFile(uuid, src);
    else if (src instanceof Buffer)   return setFromBuffer(uuid, src, opts);
};

Project.prototype.parkingUuids = function(){
    let dir = this._subpath('parkings');
    this._ensure_dir(dir);
    if (!fs.existsSync(dir)) {
        return [];
    } else {
        return fs.readdirSync(dir)
            .filter(f => f.match(/\.json$/))
            .map(f => new parking.Uuid(f.replace(/\.json$/, '')));
    }
};
Project.prototype.getParkingByUuid = function(uuid){
    let f = this._subpath('parkings', uuid.toData() + '.json');
    this._ensure_parent(f);
    if (fs.existsSync(f)) {
        let a = JSON.parse(fs.readFileSync(f, 'utf8').toString());
        let b = parking.Parking.fromData(a);
        if (uuid.toData() !== b.getUuid().toData())
            throw new Error('filename ' + f +
                ' doesn\'t match uuid of parking ' + b.getUuid().toData());
        return b;
    } else {
        return null;
    }
};
Project.prototype.setParking = function(parking){
    let uuid = parking.getUuid();
    if (!uuid)
        throw new Error('parking must have a uuid when using setParking');
    let f = this._subpath('parkings', uuid.toData() + '.json');
    this._ensure_parent(f);
    fs.writeFileSync(f, JSON.stringify(parking.toData(), null, 4) + os.EOL);
    return null;
};
Project.prototype.removeParkingByUuid = function(uuid){
    let f = this._subpath('parkings', uuid.toData() + '.json');
    if (fs.existsSync(f))
        fs.rmSync(f);
    return null;
};

Project.prototype.areaUuids = function(){
    let dir = this._subpath('areas');
    this._ensure_dir(dir);
    if (!fs.existsSync(dir)) {
        return [];
    } else {
        return fs.readdirSync(dir)
            .filter(f => f.match(/\.json$/))
            .map(f => new area.Uuid(f.replace(/\.json$/, '')));
    }
};
Project.prototype.getAreaByUuid = function(uuid){
    let f = this._subpath('areas', uuid.toData() + '.json');
    this._ensure_parent(f);
    if (fs.existsSync(f)) {
        let a = JSON.parse(fs.readFileSync(f, 'utf8').toString());
        let b = area.Area.fromData(a);
        if (uuid.toData() !== b.getUuid().toData())
            throw new Error('filename ' + f +
                ' doesn\'t match uuid of area ' + b.getUuid().toData());
        return b;
    } else {
        return null;
    }
};
Project.prototype.setArea = function(area){
    let uuid = area.getUuid();
    if (!uuid)
        throw new Error('area must have a uuid when using setArea');
    let f = this._subpath('areas', uuid.toData() + '.json');
    this._ensure_parent(f);
    fs.writeFileSync(f, JSON.stringify(area.toData(), null, 4) + os.EOL);
    return null;
};
Project.prototype.removeAreaByUuid = function(uuid){
    let f = this._subpath('areas', uuid.toData() + '.json');
    if (fs.existsSync(f))
        fs.rmSync(f);
    return null;
};


module.exports = {
    Project: Project,
};
