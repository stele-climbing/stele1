# support-flexible-grading-systems (15)

grading systems can be odd:

- a route just in the Y.D.S. can be 5.10, 5.10+, 5.10d, 5.10/5.11 which can all be about the same
- these can kind of compare to the E grades
- E grades can somewhat compare to Movie grades, but Movie doesn't relate to Y.D.S. at all
- Hueco's fluttering heart is nice too
- does quality count as "grading"?

encoding/comparing these is a difficult task

- discipline - free, aid, ice
- style - top-rope, sport, trad, free-solo
- technical difficulty -

the best solution might be to establish a few common scales and always deliver the scale and grade:

- <topic>:<scale>:<grade>
    - Experience:British:E10
    - Difficulty:Y.D.S.:5.10+
    - Difficulty:Yosemite-Decimal-System:5.10+
    - Difficulty:Aid:A5
    - Difficulty:Clean-Aid:C5
    - Safety:Movie:G
        - !!! safety depends on the style of ascent
    - Safety:Fluttering-Hearts:3
        - 💙💙💙
        - ❤❤❤
        - 💓💓💓
        - !!! safety depends on the style of ascent

## examples

<!-- brief, self-contained example(s). if applicable -->

## motivation

<!-- why is it useful? -->

## design/implementation

## drawbacks

<!-- does it require breaking changes? -->

<!-- is this an half-measure for to address something larger? -->

<!-- what complexity does it introduce? -->

## alternatives

<!-- what other options have been considered? -->

<!-- what effect does not implemementing this have? -->

## unresolved aspects

<!-- are there open questions or parts of this approach/feature that are unknown? -->
