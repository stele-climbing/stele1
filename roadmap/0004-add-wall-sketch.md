# add wall sketch (4)

- systems: corners, cracks, flakes, ledges
- features: blocks, anchors, nests, trees

a few purposes:

- big wall navigation - only the features that intersect with a route
- crag identification - includes many features to help the climber identify the cliff
- on-photo layer - an opacified, washed out photo with the topo drawn on top

## example

- [A wall at The pit](https://cdn-files.apstatic.com/climb/106371646_large_1494102550.jpg)
  (from Mountain Project)

## motivation

<!-- why is it useful? -->

## design/implementation

## drawbacks

<!-- does it require breaking changes? -->

<!-- is this an half-measure for to address something larger? -->

<!-- what complexity does it introduce? -->

## alternatives

<!-- what other options have been considered? -->

<!-- what effect does not implemementing this have? -->

## unresolved aspects

<!-- are there open questions or parts of this approach/feature that are unknown? -->
