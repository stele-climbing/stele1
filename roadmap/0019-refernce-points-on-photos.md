# refernce points on photos (19)

<!-- introduction and brief summary -->

pin coordinates on a photo, to a real longitude/latitude.
this would help build a model of the area, and suggest photos for drawing.

## examples

<!-- brief, self-contained example(s). if applicable -->

## motivation

<!-- why is it useful? -->

## design/implementation

## drawbacks

<!-- does it require breaking changes? -->

<!-- is this an half-measure for to address something larger? -->

<!-- what complexity does it introduce? -->

## alternatives

<!-- what other options have been considered? -->

<!-- what effect does not implemementing this have? -->

## unresolved aspects

<!-- are there open questions or parts of this approach/feature that are unknown? --> 
