# support photo layers (3)

photos should have more than just climbs and areas on them

- boxes
- arrows
- hardware (age and notes?)
  - bolts
  - anchors
  - hangers
  - pins
  - slings
  - rap-stations
- measurements
- warnings

these layers should have the opportunity to be community-editable or personal

## motivation

<!-- why is it useful? -->

## design/implementation

## drawbacks

<!-- does it require breaking changes? -->

<!-- is this an half-measure for to address something larger? -->

<!-- what complexity does it introduce? -->

## alternatives

<!-- what other options have been considered? -->

<!-- what effect does not implemementing this have? -->

## unresolved aspects

<!-- are there open questions or parts of this approach/feature that are unknown? -->
