# climbing-area types (7)

different organization levels could be useful:

- region - state, west coast, Southwest California, central east coast, etc.
- area - used for grouping stuff
  - e.g.: redrocks, yosemite, el-cap, moro rock, roadside
- feature/rock/formation - an easily-identifiable section of rock/area
- district - groups stuff by the approach 
  - in new jack city box canyon east and west 
    divide features on the ridge by where you approach from
    this means that two adjacent routes are in entirely separate districts
- sector
   - arbitrary divisions that aren't easily drawn on a map
     - sections of a long wall (like endless at the new river gorge)
     - sections of a homogenous boulder field

## motivation

<!-- why is it useful? -->

## design/implementation

## drawbacks

<!-- does it require breaking changes? -->

<!-- is this an half-measure for to address something larger? -->

<!-- what complexity does it introduce? -->

## alternatives

<!-- what other options have been considered? -->

<!-- what effect does not implemementing this have? -->

## unresolved aspects

<!-- are there open questions or parts of this approach/feature that are unknown? -->
