# anchor placements and fixed gear (18)

<!-- introduction and brief summary -->

## examples

<!-- brief, self-contained example(s). if applicable -->

## motivation

<!-- why is it useful? -->

## design/implementation

## drawbacks

<!-- does it require breaking changes? -->

<!-- is this an half-measure for to address something larger? -->

<!-- what complexity does it introduce? -->

## alternatives

<!-- what other options have been considered? -->

<!-- what effect does not implemementing this have? -->

## unresolved aspects

<!-- are there open questions or parts of this approach/feature that are unknown? --> 

- should they be free-form, or rely on some central enforcable/checkable model?
