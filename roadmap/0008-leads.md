# Leads (8)

inspired by what you'll find in a botanical guide, 
"leads" are a series of contrasting statements that help you narrow down what you're looking at.

this cuts down on a climb's description needing to describe the climb.

## example

here's what a lead might look like:

- 1 anchors are visible from the base of the route
  - 2 the route ends on a large ledge
  - 2 the route ends under a roof
- 1 anchors are not visible from the base of the route
  - 2 the route climbs a right-facing corner
    - 2 there is a bolt halfway up the corner
    - 2 the corner has no bolts
  - 2 there is microwave-sized hueco at the fourth bolt

## motivation

why is it useful?

## design/implementation

## drawbacks

it's fairly hard to pin down.
without aid, it would take someone who knows the area well to put together a good lead.

could seem foreign to many climbers

even when implemented successfully,
it might not be concise and/or clear enough to have a strong advange over a series of photos

## alternatives

<!-- what other options have been considered? -->

<!-- what effect does not implemementing this have? -->

## unresolved aspects

are there open questions or parts of this approach/feature that are unknown?

this could be well-aided by a simple table where someone can enter statements and identify climbs that match the statement.
this would save developers from needing to imagine every possible option;
they can define statements, and answer them about climbs, letting the sorting/grouping be done automatically.
