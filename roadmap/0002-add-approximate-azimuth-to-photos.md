# add approximate azimuth to photos (2)


with a small pair of points, 
an approximate azimuth and field of view could be available for the photo.

a photo could be be accompanied by pairs of points:

- one point for the coordinates on a photo
- a corresponding point for the coordinates on the earth

With one pair, (and the location the photo was taken from) it'd be possible to get a general azimuth.
With two pairs there would be an azimuth and an general field of view.

## motivation

<!-- why is it useful? -->

## design/implementation

## drawbacks

<!-- does it require breaking changes? -->

<!-- is this an half-measure for to address something larger? -->

<!-- what complexity does it introduce? -->

## alternatives

<!-- what other options have been considered? -->

<!-- what effect does not implemementing this have? -->

## unresolved aspects

<!-- are there open questions or parts of this approach/feature that are unknown? -->
