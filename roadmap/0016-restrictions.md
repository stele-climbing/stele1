# restrictions (16)

<!-- introduction and brief summary -->

## examples

<!-- brief, self-contained example(s). if applicable -->

## motivation

<!-- why is it useful? -->

## design/implementation

- useful links for a more robust implementation:
  - https://zachholman.com/talk/utc-is-enough-for-everyone-right#recurring-events
  - https://martinfowler.com/apsupp/recurring.pdf

## drawbacks

<!-- does it require breaking changes? -->

<!-- is this an half-measure for to address something larger? -->

<!-- what complexity does it introduce? -->

## alternatives

<!-- what other options have been considered? -->

<!-- what effect does not implemementing this have? -->

## unresolved aspects

<!-- are there open questions or parts of this approach/feature that are unknown? --> 
