# expand safety ratings (1)

top level categories

- style/pro
  - (trad (same as mixed!), sport, boulder, top-rope, or any text?)
  - (gear, bolts, knots, pads, top-rope, screws) --- this is probably a bad approach
- grade
  - any text with suggestions
    - movie (G, PG, ...)
    - english (frequent protection, infrequent pro, unavoidable injury, ...)
- details
  - any text
    - something like with an anchored belayer or with double rope to avoid sharp arete

## motivation

<!-- why is it useful? -->

## design/implementation

## drawbacks

<!-- does it require breaking changes? -->

<!-- is this an half-measure for to address something larger? -->

<!-- what complexity does it introduce? -->

## alternatives

<!-- what other options have been considered? -->

<!-- what effect does not implemementing this have? -->

## unresolved aspects

<!-- are there open questions or parts of this approach/feature that are unknown? -->
