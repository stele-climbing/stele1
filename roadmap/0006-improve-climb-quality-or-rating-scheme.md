# improve climb-quality rating scheme (6)

there are some options:

- stars (0-5, maybe halfs)
  - used widely
- simple quality (0-100)
  - precise, unique to stele1, easily translated to stars
- "worth if..."
  - delivers a good message
  - if...
    - within walking distance
    - in the area
    - in the region
    - within driving distance
    - make the trip for just this climb!
- context stars
  - regional stars
  - area stars
  - crag stars
- should i climb it (series of y/n questions)
  - no matter where you are
  - if you're within driving distance
  - if you're within walking distance

a good approach:

- user's sort climbs within an area by quality/like/glad-they-did-it/would-recommend
- the highest is 100, lowest is 0. this ensures that climbs are graded relatively.
- "best" climbs are identified based on some heuristic (undecided)
- maybe group by category
  - the best are candidates for the next level of sorting
  - repeat with the next higher area

upper climbs will be placed with more attention to detail than lower ones (a log scale could be useful).

maybe vertical slots? 
so climbs can be compared apples-to-apples (probably too much work for little payoff)
this has some benefits:

- avoids crag-relative grades on an absolute scale a 5* at dirty local crag probably won't be as good as a 5* at the NRG.
- lets users do user-relative stuff (only rate climbs done against climbs done)

## motivation

<!-- why is it useful? -->

## design/implementation

## drawbacks

<!-- does it require breaking changes? -->

<!-- is this an half-measure for to address something larger? -->

<!-- what complexity does it introduce? -->

## alternatives

<!-- what other options have been considered? -->

<!-- what effect does not implemementing this have? -->

## unresolved aspects

<!-- are there open questions or parts of this approach/feature that are unknown? -->
