#!/usr/bin/env python3

#
# this makes the data of the datatypes and filesystem available as python data structures.
#
# the main use of this is to generate tests/code/docs based on the current state of the spec.
#
# if you find yourself saying "i want to use file(s) from the spec directory",
# then there's a fair chance this script could help you
#

import os
import os.path
import json

class Identifier:
    def __init__(self, singular):
        self.kebab = Identifier.kebab(singular)
        self.snake = Identifier.snake(singular)
        self.camel = Identifier.camel(singular)
        self.pascal = Identifier.pascal(singular)
    @staticmethod
    def kebab(words):
        return str.join('-', [w.lower() for w in words])
    @staticmethod
    def snake(words):
        return str.join('_', [w.lower() for w in words])
    @staticmethod
    def pascal(words):
        return str.join('', [w.capitalize() for w in words])
    @staticmethod
    def camel(words):
        p = Identifier.pascal(words)
        return p[:1].lower() + p[1:] 

def filename_without_extension(n):
    return str.split(os.path.basename(n), '.')[0]

def read_json_file(path):
    try:
        with open(path, 'r') as infile:
            return json.load(infile)
    except json.decoder.JSONDecodeError as e:
        raise Exception("bad JSON file " + path + ' ' + e.args[0])

def enumerate_extra(e, start=0):
    """wraps an object that supports iteration
    and yields quads of (value, is_first, is_last, index)"""
    idx = start
    i = iter(e)
    try:
        pending = next(i) 
        for nxt in i:
            yield (pending, idx == start, False, idx)
            pending = nxt
            idx += 1
        yield (pending, idx == start, True, idx)
    except StopIteration:
        pass


# TODO: there's overlap between the "is_template"
#       and project-level singletons.
#       is there a good way to handle both at the same time?
class Entry:
    def __init__(self, name, filetype, is_template, description, contents=None):
        self.name = name;
        self.filetype = filetype;
        self.is_template = is_template;
        self.description = description;
        self.contents = contents;
    def is_file(self):
        return self.filetype == 'file'
    def is_dir(self):
        return self.filetype == 'directory'
    @staticmethod
    def from_data(d):
        name = d['name']
        filetype = d['type'].split(':')[0]
        is_template = d['type'].endswith('template')
        description = d['description']
        if filetype == 'directory':
            contents = []
            if 'children' in d:
                for child in d['children']:
                    contents.append(Entry.from_data(child))
            return Entry(name, filetype, is_template, description, contents)
        else:
            return Entry(name, filetype, is_template, description)

# FIXME: this is ordered, but allows for conflicting file names
class Filesystem:
    def __init__(self):
        self._entries = []
    def add_entry(self, e):
        self._entries.append(e)
    def list_entries(self):
        def walk(e, p):
            yield (e, p + [e.name])
            if e.filetype == 'directory' and e.contents:
                for child in e.contents:
                    for a in walk(child, p + [e.name]):
                       yield a
        for re in self._entries:
            for (e, p) in walk(re, []):
               yield (e, p)
    def entries(self):
        return self._entries
    @staticmethod
    def from_filesystem(path):
        s = Filesystem()
        for d in read_json_file(os.path.join(path, 'filesystem.json')):
            s.add_entry(Entry.from_data(d))
        return s

class Attribute:
    def __init__(self):
         self.examples = None
         self.description = None
         self.name = None
         self.id = None
    @staticmethod
    def from_filesystem(path, eslug, aslug):
        a = Attribute()
        datafile = os.path.join(path, eslug, aslug) + '.json'
        d = read_json_file(datafile)
        human_name = d['name']
        a.name = d['name']
        a.id = Identifier(str.split(a.name, ' ')) 
        expected_aslug = a.id.kebab
        if not aslug == expected_aslug:
             expected_path = os.path.join(path, eslug, expected_aslug) + '.json'
             raise Exception(
                  f'attribute name "{human_name}" does not match its path: ' +
                  f'found at {datafile}, but expected to be at "{expected_path}"'
             )
        a.examples = d['examples']
        a.description = str.join(os.linesep, d['description'])
        return a

class Entity:
    def __init__(self):
         self.name = None
         self._attributes = []
    def add_attribute(self, attr):
        self._attributes.append(attr)
    def attributes(self):
        return sorted(self._attributes, key=lambda a: a.name)
    @staticmethod
    def from_filesystem(path, eslug):
        e = Entity()
        edir = os.path.join(path, eslug)
        edata = os.path.join(path, eslug) + '.json'
        d = read_json_file(edata)
        human_name = d['name']
        e.name = d['name']
        e.plural_name = d['pluralName']
        e.id = Identifier(str.split(e.name, ' ')) 
        e.plural_id = Identifier(str.split(e.plural_name, ' ')) 
        expected_eslug = e.id.kebab
        if not eslug == expected_eslug:
             expected_path = os.path.join(path, expected_slug) + '.json'
             raise Exception(
                  f'entity name "{human_name}" does not match its path: ' +
                  f'found at {edata}, but expected to be at "{expected_path}"'
             )
        for af in os.listdir(edir):
            aslug = filename_without_extension(af)
            attr = Attribute.from_filesystem(path, eslug, aslug)
            e.add_attribute(attr)
        return e

class Datatypes:
    def __init__(self):
        self._entities = []
    def add_entity(self, ent):
        self._entities.append(ent)
    def entities(self):
        return [e for e in self._entities]
    @staticmethod
    def from_filesystem(path):
        d = Datatypes()
        if not os.path.isdir(path):
            raise Exception("no directory at path " + path)
        for a in os.listdir(path):
            if os.path.isdir(os.path.join(path, a)):
                d.add_entity(Entity.from_filesystem(path, a))
        return d
    def get_entity(self, name):
        for e in self._entities:
            if e.id.kebab == name:
                return e

if __name__ == "__main__":
    import sys
    dtspec = Datatypes.from_filesystem("./spec/datatypes/")
    fsspec = Filesystem.from_filesystem("./spec/filesystem/")
    print('DATATYPES:')
    for e in dtspec.entities():
        attr_names = str.join(', ', [a.id.kebab for a in e.attributes()])
        print(f'    {e.id.kebab}')
        print(f'        {attr_names}')
    print('FILESYSTEM:')
    for (e, p) in fsspec.list_entries():
        indent = '   ' * len(p)
        suffix = '/' if e.filetype == 'directory' else ''
        print(f'{indent}{e.name}{suffix} :: {e.description}')
