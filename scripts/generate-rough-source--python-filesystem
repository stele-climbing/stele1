#!/usr/bin/env python3

import jinja2
import spec

template = '''import shutil
import os
import os.path
import json

{% for e in cspec.entities() %}\
import {{require_path}}.{{e.id.snake}} as {{e.id.snake}}
{% endfor %}\

class Project:

    def __init__(self, path):
        self._path_original = path;
        self._path = os.path.abspath(path);
    def _subpath(self, *segments):
        return os.path.join(self._path, *segments)
    def _ensure_dir(self, path):
        if not os.path.isdir(path):
            os.mkdir(path)
        return None
    def _ensure_parent_dir(self, path):
        return self._ensure_dir(os.path.dirname(path))

    def exists(self):
        isfile = os.path.isfile
        metafile = self._subpath('project.json')
        versionfile = self._subpath('stele1.txt')
        return isfile(metafile) and isfile(versionfile)

    def create(self):
        parent = os.path.dirname(self._path)
        if os.path.isdir(self._path):
            raise Exception(f'directory exists {self._path}')
        if not os.path.isdir(parent):
            raise Exception(f'parent directory missing {parent} (for {self._path})')
        os.mkdir(self._path)
{% for e in cspec.entities() %}\
{% if e.id.kebab != 'metadata' %}\
        os.mkdir(self._subpath('{{e.plural_id.kebab}}'))
{% endif %}\
{% endfor %}\
        with open(os.path.join(self._path, 'stele1.txt'), 'w') as f:
            f.write('stele1' + os.linesep)
        self.set_metadata(metadata.Metadata())

    def get_metadata(self):
        metapath = self._subpath('project.json')
        if os.path.isfile(metapath):
            with open(metapath, 'r') as f:
                try:
                    return metadata.Metadata.from_data(json.load(f))
                except json.decoder.JSONDecodeError as e:
                    raise Exception(f'bad json at {pat}: {e.args[0]}')
        else:
            return None
    def set_metadata(self, metadata):
        metapath = self._subpath('project.json')
        if os.path.isdir(os.path.dirname(metapath)):
            with open(metapath, 'w') as f:
                json.dump(metadata.to_data(), f, indent=4, ensure_ascii=False)
                f.write(os.linesep)
        else:
            return None
{% for e in cspec.entities() %}\
{% if e.id.kebab != 'metadata' %}\

    def {{e.id.snake}}_uuids(self):
        dir = self._subpath('{{e.plural_id.kebab}}')
        if os.path.isdir(dir):
            for f in os.listdir(dir):
                if f.endswith('.json'):
                    yield {{e.id.snake}}.Uuid.from_data(f.replace('.json', ''))
    def get_{{e.id.snake}}_by_uuid(self, uuid):
        pat = self._subpath('{{e.plural_id.kebab}}', uuid.to_data() + '.json')
        if os.path.isfile(pat):
            with open(pat, 'r') as f:
                try:
                    e = {{e.id.snake}}.{{e.id.pascal}}.from_data(json.load(f)) 
                    if uuid.to_data() != e.get_uuid().to_data():
                        raise Exception(f'path {pat} does not match the uuid {e.get_uuid().to_data()}')
                    return e
                except json.decoder.JSONDecodeError as e:
                    raise Exception(f'bad json at {pat}: {e.args[0]}')
    def set_{{e.id.snake}}(self, {{e.id.snake}}):
        uuid = {{e.id.snake}}.get_uuid()
        if not uuid:
            raise Exception('can not use {{e.id.kebab}} without a uuid')
        pat = self._subpath('{{e.plural_id.kebab}}', {{e.id.snake}}.get_uuid().to_data() + '.json')
        self._ensure_parent_dir(pat)
        with open(pat, 'w') as f:
            json.dump({{e.id.snake}}.to_data(), f, indent=4, ensure_ascii=False)
            f.write(os.linesep)
    def remove_{{e.id.snake}}_by_uuid(self, uuid):
        pat = self._subpath('{{e.plural_id.kebab}}', uuid.to_data() + '.json')
{% if e.id.kebab == 'photo' %}\
        imgpat = self._get_photo_image_path(uuid)
{% endif %}\
        if os.path.isfile(pat):
            os.unlink(pat)
{% if e.id.kebab == 'photo' %}\
        if os.path.isfile(imgpat):
            os.unlink(imgpat)
{% endif %}\
{% if e.id.kebab == 'photo' %}\
    def _get_photo_image_path(self, uuid):
        dir = self._subpath('photos')
        prefix = uuid.to_data() + '_image'
        if os.path.isdir(dir):
            for f in os.listdir(dir):
                if f.startswith(prefix):
                    return os.path.join(dir, f)
    def set_photo_image_by_uuid(self, uuid, path):
        if not os.path.isfile(path):
            raise Exception('no file at path ' + path)
        dir = self._subpath('photos')
        existing = self._get_photo_image_path(uuid)
        ext = os.path.basename(path).split('.')[-1]
        suffix = '.' + ext if ext else ''
        if existing:
            # TODO: warn the user because this is a terrible idea
            os.unlink(existing)
        shutil.copyfile(path, os.path.join(dir, f'{uuid.to_data()}_image{suffix}'))
    def get_photo_image_by_uuid(self, uuid):
        return self._get_photo_image_path(uuid)
{% endif %}\
{% endif %}\
{% endfor %}\

if __name__ == '__main__':
    import stele1.filesystemcli
    stele1.filesystemcli.main()
'''

print(jinja2.Template(template).render(
    require_path = "stele1",
    spec         = spec,
    cspec        = spec.Datatypes.from_filesystem('./spec/datatypes'),
    fsspec       = spec.Filesystem.from_filesystem('./spec/filesystem')
))

